# DatKlik- Vaš grafični prikazovalnik Javnih podatkov Slovenije
## Sledeč repozitorij vsebuje DatKlik aplikacijo
---
## Trenutna različica: 1.0.0

## Logo:
![Slika logotipa](OPSI/src/main/resources/static/img/logotip2.png)

## Ideja DatKlika
DatKlik omogoča pregled in grafični prikaz dveh podatkovnih zbirk iz Javnih podatkov Slovenije, 
kateri prikazujeta diagnoze medicinskih oskrb glede na regijo in izvajalce.
Obe zbirki imata enako vrstično skalo, kar nam omogoča da iz njih delamo tudi preseke
in tako prikazujemo novo naučeno znanje.

### Funkcionalnosti:
1. Predefinirane zanimive diagnoze na začetni strani
2. Možnost iskanja po zbirkah in diagnozah
3. Možnost izvoza grafa v .jpg obliki
4. Prikaz več vrst grafov (tortni in stolšični)
5. Responsive design po komplet strani

## Arhitekturna zasnova aplikacije:
![Slika arhitekture](OPSI/src/main/resources/static/img/Praktikum_2_arhitekt.png)

## Uporabljene knjižnice :
- Chart.js - za vizualizacijo in interakcijo z grafi [Link](https://www.chartjs.org/)
- Popper - za prikaz "Tooltip"-ov na domači strani [Link](https://popper.js.org/)
- Select2 - za hitrejše iskanje po dropdown-u [Link](https://select2.org/)
- P5.js - za animacijo na domači strani [Link](https://github.com/processing/p5.js)
- Bootstrap - za responsive design [Link](https://getbootstrap.com/)

----
### Povezava do aplikacije:
[Link](https://datklik.azurewebsites.net/?fbclid=IwAR3caXtDjhrv7M-7LuqJycY1a7TD_QRJ8SAcQOLvy1uvfVF23isha1YwYzc)

## Podatki so povzeti po strani :
https://podatki.gov.si/

### Povezave do obeh podatkovnih zbirk :
[SPP primeri po statističnih regijah](https://podatki.gov.si/dataset/nijzspp_tb15a)
[SPP primeri po izvajalcih ZV](https://podatki.gov.si/dataset/nijzspp_tb07a)
>_Zajeti podatki se uporabljajo pod licenco_ : CC BY 4.0

## Razvojno okolje in orodja:
- Intellij IDEA Ultimate 2020.1
- MongoDB



