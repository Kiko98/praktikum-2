Osnovni standardi pri programiranju:
1. Ne commit-aj in push-aj, če nisi prepričan. Raje se pozanimaj pri komu, če je
   vsebinsko vredi za commit in push.
2. Pri vsakem commit-u pod message oz. sporočilo commit-a pred vsebino sporočila
   vneseš kateri številčni korak je, če ne veš na pamet kateri je naslednji, 
   greš na GitLab pod zavihek "Repository" in podzavihek "Commits", kjer imaš 
   prikazane vse oštevilčene commit-e.
3. Nad vsako metodo obvezno zakomentiraj kodo na sledeči način (primer):

 /**
 * Opis metode
 *
 * @param  prvi parameter metode (če je seveda...)
 *
 * @param  drugi parameter metode (če je seveda...)
 *
 * @return vrednost, ki jo vračamo
 */
 
4. Brez duplicirane kode, čisto programiraj in čim bolj na kratko če da seveda.