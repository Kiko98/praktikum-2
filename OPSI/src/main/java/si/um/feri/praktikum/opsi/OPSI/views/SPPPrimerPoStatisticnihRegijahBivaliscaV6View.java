package si.um.feri.praktikum.opsi.OPSI.views;

import lombok.Getter;
import lombok.Setter;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoStatisticnihRegijahBivaliscaV6;
import si.um.feri.praktikum.opsi.OPSI.views.helpers.Pomagac;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Getter
@Setter
public class SPPPrimerPoStatisticnihRegijahBivaliscaV6View {

    private List<String> imenaStatisticnihRegijBivalisca;

    private Pomagac pomagac;

    public SPPPrimerPoStatisticnihRegijahBivaliscaV6View() {
        this.imenaStatisticnihRegijBivalisca = new ArrayList<>();
        this.pomagac = new Pomagac();
        napolniStatisticneRegijeBivaliscaVList();
    }

    /**
     * Ob inicializaciji objekta uporabimo to metodo, da napolnimo list z imeni vseh regij..
     */
    private void napolniStatisticneRegijeBivaliscaVList() {
        this.imenaStatisticnihRegijBivalisca.add("Skupaj");
        this.imenaStatisticnihRegijBivalisca.add("Pomurska");
        this.imenaStatisticnihRegijBivalisca.add("Podravska");
        this.imenaStatisticnihRegijBivalisca.add("Koroška");
        this.imenaStatisticnihRegijBivalisca.add("Savinjska");
        this.imenaStatisticnihRegijBivalisca.add("Zasavska");
        this.imenaStatisticnihRegijBivalisca.add("Posavska");
        this.imenaStatisticnihRegijBivalisca.add("Jugovzhodna Slovenija");
        this.imenaStatisticnihRegijBivalisca.add("Osrednjeslovenska");
        this.imenaStatisticnihRegijBivalisca.add("Gorenjska");
        this.imenaStatisticnihRegijBivalisca.add("Primorsko-notranjska");
        this.imenaStatisticnihRegijBivalisca.add("Goriška");
        this.imenaStatisticnihRegijBivalisca.add("Obalno-kraška");
        this.imenaStatisticnihRegijBivalisca.add("Tujina");
        this.imenaStatisticnihRegijBivalisca.add("Neznano");
    }

    /**
     * Metoda s pomočjo pomožne metode "preveriCeJeVrednostStevilo" preverja, če so vrednosti atributov (atributi
     * so listi, kličemo pa prvi niz v listu) podanega objekta števila ali ne. V primeru, da je vrednost niza število,
     * shrani regijo, ki se nanaša na določen atribut iz lista "imenaStatisticnihRegijBivalisca" v list "validnaImenaRegij".
     *
     * @param bivaliscaV6 tipa SPPPrimerPoStatisticnihRegijahBivaliscaV6
     * @return validnaImenaRegij tipa List<String>
     */
    public List<String> vrniListValidnihRegij(SPPPrimerPoStatisticnihRegijahBivaliscaV6 bivaliscaV6) {
        List<String> validnaImenaRegij = new ArrayList<>();
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getSkupaj().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(0));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getPomurska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(1));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getPodravska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(2));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getKoroska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(3));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getSavinjska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(4));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getZasavska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(5));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getPosavska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(6));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getJugovzhodnaSlovenija().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(7));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getOsrednjeslovenska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(8));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getGorenjska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(9));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getPrimorskoNotranjska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(10));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getGoriska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(11));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getObalnoKraska().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(12));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getTujina().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(13));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getNeznano().get(0)))
            validnaImenaRegij.add(imenaStatisticnihRegijBivalisca.get(14));
        return validnaImenaRegij;
    }

    /**
     * Metoda ustvari HashMap<String, Integer>, kot niz shrani ime izvajalca iz lista "imenaStatisticnihRegijBivalisca",
     * kot število pa pretvori vrednost atributa (prvi niz lista), ki se nanaša na regijo iz objekta iz niza v število.
     * Ko je HashMap ustvarjen ga s pomočjo metode "vrniListVrednostiIzbranihVnosov" iteriramo, primerjamo imena regij
     * iz lista "izbraneStatisticneRegijeBivalisca" in imena regij v HashMapu, če so imena ista shranimo številčne
     * vrednosti tega imena iz HashMap-a v list "vrednostiIzbranihVnosov" v pomožni metodi in list vrnemo.
     *
     * @param bivaliscaV6                       tipa SPPPrimerPoStatisticnihRegijahBivaliscaV6
     * @param izbraneStatisticneRegijeBivalisca tipa List<String>
     * @return List<Integer>
     */
    public List<Integer> vrniVrednostiIzbranihStatisticnihRegijBivalisca(SPPPrimerPoStatisticnihRegijahBivaliscaV6 bivaliscaV6,
                                                                         List<String> izbraneStatisticneRegijeBivalisca) {
        LinkedHashMap<String, Integer> vrednostiStatisticnihRegijBivaliscaHashMp = new LinkedHashMap<>();
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getSkupaj().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(0),
                    Math.round(Float.parseFloat(bivaliscaV6.getSkupaj().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getPomurska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(1),
                    Math.round(Float.parseFloat(bivaliscaV6.getPomurska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getPodravska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(2),
                    Math.round(Float.parseFloat(bivaliscaV6.getPodravska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getKoroska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(3),
                    Math.round(Float.parseFloat(bivaliscaV6.getKoroska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getSavinjska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(4),
                    Math.round(Float.parseFloat(bivaliscaV6.getSavinjska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getZasavska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(5),
                    Math.round(Float.parseFloat(bivaliscaV6.getZasavska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getPosavska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(6),
                    Math.round(Float.parseFloat(bivaliscaV6.getPosavska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getJugovzhodnaSlovenija().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(7),
                    Math.round(Float.parseFloat(bivaliscaV6.getJugovzhodnaSlovenija().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getOsrednjeslovenska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(8),
                    Math.round(Float.parseFloat(bivaliscaV6.getOsrednjeslovenska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getGorenjska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(9),
                    Math.round(Float.parseFloat(bivaliscaV6.getGorenjska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getPrimorskoNotranjska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(10),
                    Math.round(Float.parseFloat(bivaliscaV6.getPrimorskoNotranjska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getGoriska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(11),
                    Math.round(Float.parseFloat(bivaliscaV6.getGoriska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getObalnoKraska().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(12),
                    Math.round(Float.parseFloat(bivaliscaV6.getObalnoKraska().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getTujina().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(13),
                    Math.round(Float.parseFloat(bivaliscaV6.getTujina().get(0))));
        if (pomagac.preveriCeJeVrednostStevilo(bivaliscaV6.getNeznano().get(0)))
            vrednostiStatisticnihRegijBivaliscaHashMp.put(imenaStatisticnihRegijBivalisca.get(14),
                    Math.round(Float.parseFloat(bivaliscaV6.getNeznano().get(0))));
        return pomagac.vrniListVrednostiIzbranihVnosov(
                vrednostiStatisticnihRegijBivaliscaHashMp, izbraneStatisticneRegijeBivalisca);
    }

}
