package si.um.feri.praktikum.opsi.OPSI.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoIzvajalcihZVInSkupinahSPPV6;
import si.um.feri.praktikum.opsi.OPSI.repositories.SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Repository;

import java.util.List;

@Service
public class SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Service {

    private final SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Repository repo;

    @Autowired
    public SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Service(SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Repository repo) {
        this.repo = repo;
    }

    public List<SPPPrimerPoIzvajalcihZVInSkupinahSPPV6> getAll() {
        return repo.findAll();
    }

    public void save(SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 sppPrimerPoIzvajalcihZVInSkupinahSPPV6) {
        repo.save(sppPrimerPoIzvajalcihZVInSkupinahSPPV6);
    }

    public SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 getById(Long id) {
        return repo.findById(id).get();
    }

    public void deleteById(Long id) {
        repo.deleteById(id);
    }

    public List<SPPPrimerPoIzvajalcihZVInSkupinahSPPV6> getAllOznakaInImeBolezniById(String leto) {
        return repo.getAllOznakaInImeBolezniByIdOrderByIzvajalciZVSkupajDesc(leto);
    }

    public SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 getSPPPrimerPoIzvajalcihZVInSkupinahSPPV6ByLetoAndOznakaInImeBolezni
            (String leto, String oznakaInImeBolezni) {
        return repo.getSPPPrimerPoIzvajalcihZVInSkupinahSPPV6ByLetoAndOznakaInImeBolezni(leto, oznakaInImeBolezni);
    }
}
