package si.um.feri.praktikum.opsi.OPSI.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter//avtomatsko generira getterje pa setterje
@Setter
@Document(collection = "spp_primeri_po_statisticnih_regijah_bivalisca_v6")//specifikacija kolekcije
public class SPPPrimerPoStatisticnihRegijahBivaliscaV6 implements Comparable<SPPPrimerPoStatisticnihRegijahBivaliscaV6> {

    @Transient
    public final String IME_SEKVENCE = "spp_primeri_po_statisticnih_regijah_bivalisca_v6";

    @Id
    private Long id;
    private String leto;
    private String oznakaInImeBolezni;
    private List<String> skupaj;
    private List<String> pomurska;
    private List<String> podravska;
    private List<String> koroska;
    private List<String> savinjska;
    private List<String> zasavska;
    private List<String> posavska;
    private List<String> jugovzhodnaSlovenija;
    private List<String> osrednjeslovenska;
    private List<String> gorenjska;
    private List<String> primorskoNotranjska;
    private List<String> goriska;
    private List<String> obalnoKraska;
    private List<String> tujina;
    private List<String> neznano;

    public SPPPrimerPoStatisticnihRegijahBivaliscaV6(String leto, String oznakaInImeBolezni, List<String> skupaj,
                                                     List<String> pomurska, List<String> podravska, List<String> koroska,
                                                     List<String> savinjska, List<String> zasavska, List<String> posavska,
                                                     List<String> jugovzhodnaSlovenija, List<String> osrednjeslovenska,
                                                     List<String> gorenjska, List<String> primorskoNotranjska,
                                                     List<String> goriska, List<String> obalnoKraska, List<String> tujina,
                                                     List<String> neznano) {
        this.leto = leto;
        this.oznakaInImeBolezni = oznakaInImeBolezni;
        this.skupaj = skupaj;
        this.pomurska = pomurska;
        this.podravska = podravska;
        this.koroska = koroska;
        this.savinjska = savinjska;
        this.zasavska = zasavska;
        this.posavska = posavska;
        this.jugovzhodnaSlovenija = jugovzhodnaSlovenija;
        this.osrednjeslovenska = osrednjeslovenska;
        this.gorenjska = gorenjska;
        this.primorskoNotranjska = primorskoNotranjska;
        this.goriska = goriska;
        this.obalnoKraska = obalnoKraska;
        this.tujina = tujina;
        this.neznano = neznano;
    }

    @Override
    public int compareTo(SPPPrimerPoStatisticnihRegijahBivaliscaV6 bivaliscaV6) {
        int primerjava = Math.round(Float.parseFloat(bivaliscaV6.getSkupaj().get(0)));
        return Math.round(Float.parseFloat(this.getSkupaj().get(0))) - primerjava;
    }

}
