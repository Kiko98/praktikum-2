package si.um.feri.praktikum.opsi.OPSI.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import si.um.feri.praktikum.opsi.OPSI.components.Kartica;
import si.um.feri.praktikum.opsi.OPSI.components.TabelnaVrstica;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoIzvajalcihZVInSkupinahSPPV6;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoStatisticnihRegijahBivaliscaV6;
import si.um.feri.praktikum.opsi.OPSI.services.SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Service;
import si.um.feri.praktikum.opsi.OPSI.services.SPPPrimerPoStatisticnihRegijahBivaliscaV6Service;
import si.um.feri.praktikum.opsi.OPSI.views.SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View;
import si.um.feri.praktikum.opsi.OPSI.views.SPPPrimerPoStatisticnihRegijahBivaliscaV6View;

import java.text.DecimalFormat;
import java.util.*;

@Controller
public class MainController {

    private final SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Service zvInSkupinahSPPV6Service;

    private final SPPPrimerPoStatisticnihRegijahBivaliscaV6Service statisticnihRegijahBivaliscaV6Service;

    @Autowired
    public MainController(SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Service zvInSkupinahSPPV6Service,
                          SPPPrimerPoStatisticnihRegijahBivaliscaV6Service statisticnihRegijahBivaliscaV6Service) {
        this.zvInSkupinahSPPV6Service = zvInSkupinahSPPV6Service;
        this.statisticnihRegijahBivaliscaV6Service = statisticnihRegijahBivaliscaV6Service;
    }

    /**
     * Metoda nam prikaže domačo stran s vsemi ustvarjenimi in podanimi karticami z naključnimi vrednostmi iz
     * lista podanih diagnoz.
     *
     * @return "index" tipa String
     */
    @RequestMapping("/")
    public String prikaziDomacoStran(Model model) {
        List<Kartica> kartice = new Kartica().vrniObjekteKarticZaPrikazNaDomaciStrani();
        List<TabelnaVrstica> tabelneVrstice = new TabelnaVrstica(statisticnihRegijahBivaliscaV6Service)
                .vrniPrvihDesetTabelnihVrsticDiagnozZNajvecPrimeri("2013");

        Collections.shuffle(kartice);

        model.addAttribute("kartice", kartice);
        model.addAttribute("tabelneVrstice", tabelneVrstice);
        return "index";
    }

    /**
     * Metoda za procesira ajax zahtevo in sicer za podano leto dobi 10 diagnoz z največ primeri.
     *
     * @param izbranoLeto tipa String
     * @return List<TabelnaVrstica>
     */
    @RequestMapping(value = "/posodobitev_tabele", method = RequestMethod.GET)
    @ResponseBody
    public List<TabelnaVrstica> posodobitevTabele(@RequestParam("izbranoLeto") String izbranoLeto) {
        return new TabelnaVrstica(statisticnihRegijahBivaliscaV6Service)
                .vrniPrvihDesetTabelnihVrsticDiagnozZNajvecPrimeri(izbranoLeto);
    }

    /**
     * Metoda prek modela pošlje v view potrebne podatke za prikaz in prikaže stran "iskanje_po_zbirkah.html".
     *
     * @param model tipa Model
     * @return "prikaz_po_izvajalcih_zv" tipa String
     */
    @RequestMapping("/iskanje_po_zbirkah")
    public String iskanjePoZbirkah(Model model) {
        SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View izvajalcevZVView = new SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View();
        SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 inSkupinahSPPV6 = zvInSkupinahSPPV6Service
                .getSPPPrimerPoIzvajalcihZVInSkupinahSPPV6ByLetoAndOznakaInImeBolezni("2013", "SPP skupine-SKUPAJ");


        List<SPPPrimerPoIzvajalcihZVInSkupinahSPPV6> oznakeInImenaBolezni = zvInSkupinahSPPV6Service
                .getAllOznakaInImeBolezniById("2013");
        List<String> leta = Arrays.asList("2013", "2014", "2015", "2016", "2017", "2018");
        List<String> imenaBolnic = izvajalcevZVView.vrniListImenValidnihIzvajalcevZV(inSkupinahSPPV6);

        for (SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 a : oznakeInImenaBolezni) {
            if (a.getIzvajalciZVSkupaj().equalsIgnoreCase("-")) {
                a.setIzvajalciZVSkupaj("0.0");
            }
        }

        oznakeInImenaBolezni.sort(Comparator.comparingDouble(e -> Double.valueOf(e.getIzvajalciZVSkupaj())));
        Collections.reverse(oznakeInImenaBolezni);

        model.addAttribute("oznakeInImenaBolezni", oznakeInImenaBolezni);
        model.addAttribute("leta", leta);
        model.addAttribute("imenaBolnic", imenaBolnic);

        return "iskanje_po_zbirkah";
    }


    /**
     * Metoda prek modela pošlje v view potrebne podatke za prikaz in prikaže stran "iskanje_po_diagnozah".
     *
     * @param model tipa Model
     * @return "iskanje_po_diagnozah" tipa String
     */

    @RequestMapping("/iskanje_po_diagnozah")
    public String iskanjePoDiagnozah(Model model) {
        SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View izvajalcevZVView = new SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View();
        SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 inSkupinahSPPV6 = zvInSkupinahSPPV6Service
                .getSPPPrimerPoIzvajalcihZVInSkupinahSPPV6ByLetoAndOznakaInImeBolezni("2013", "SPP skupine-SKUPAJ");

        List<SPPPrimerPoIzvajalcihZVInSkupinahSPPV6> oznakeInImenaBolezni = zvInSkupinahSPPV6Service
                .getAllOznakaInImeBolezniById("2013");
        List<String> imenaBolnic = izvajalcevZVView.vrniListImenValidnihIzvajalcevZV(inSkupinahSPPV6);

        List<String> regije = new Kartica().vrniRegijeList();

        //Array razvrstimo padajoče po številu primerov
        for (SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 a : oznakeInImenaBolezni) {
            if (a.getIzvajalciZVSkupaj().equalsIgnoreCase("-")) {
                a.setIzvajalciZVSkupaj("0.0");
            }
        }

        oznakeInImenaBolezni.sort(Comparator.comparingDouble(e -> Double.valueOf(e.getIzvajalciZVSkupaj())));
        Collections.reverse(oznakeInImenaBolezni);

        model.addAttribute("oznakeInImenaBolezni", oznakeInImenaBolezni);
        model.addAttribute("imenaBolnic", imenaBolnic);
        model.addAttribute("regije", regije);

        return "iskanje_po_diagnozah";
    }


    /**
     * Metodo uporabimo za procesiranje ajax zahteve. Kot parametra dobimo izbrano bolezen, leto in
     * izvajalce zv s katerimi iz baze pridobimo pravi objekt, ter vrnemo potrebne podatke za prikaz
     * ali posodobitev grafa na strani "iskanje_po_zbirkah.html".
     *
     * @param izbranaDiagnoza tipa String
     * @param izbranoLeto     tipa String
     * @param izbraniVnosi    tipa List<String>
     * @param izbranaZbirka   tipa String
     * @return listPotrebnihPodatkov tipa List<List>
     */
    @RequestMapping(value = "/posodobitev_grafa", method = RequestMethod.GET)
    @ResponseBody
    public List<List> posodobitevGrafa(@RequestParam("izbranaDiagnoza") String izbranaDiagnoza,
                                       @RequestParam("izbranoLeto") String izbranoLeto,
                                       @RequestParam("izbraniVnosi[]") List<String> izbraniVnosi,
                                       @RequestParam("izbranaZbirka") String izbranaZbirka) {
        List<Integer> vrednostIzbranihVnosov = new ArrayList<>();

        if (izbranaZbirka.equals("Izvajalci ZV V6")) {
            SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View zvView = new SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View();
            SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 sppv6 = zvInSkupinahSPPV6Service
                    .getSPPPrimerPoIzvajalcihZVInSkupinahSPPV6ByLetoAndOznakaInImeBolezni(izbranoLeto, izbranaDiagnoza);

            vrednostIzbranihVnosov = zvView.vrniVrednostiIzbranihIzvajalcevZV(sppv6, izbraniVnosi);
        } else if (izbranaZbirka.equals("Statistične regije bivališča V6")) {
            SPPPrimerPoStatisticnihRegijahBivaliscaV6View bivaliscaV6View = new SPPPrimerPoStatisticnihRegijahBivaliscaV6View();
            SPPPrimerPoStatisticnihRegijahBivaliscaV6 bivaliscaV6 = statisticnihRegijahBivaliscaV6Service
                    .getSPPPrimerPoStatisticnihRegijahBivaliscaV6ByLetoAndOznakaInImeBolezni(izbranoLeto, izbranaDiagnoza);

            vrednostIzbranihVnosov = bivaliscaV6View.vrniVrednostiIzbranihStatisticnihRegijBivalisca(bivaliscaV6, izbraniVnosi);
        }

        List<List> listPotrebnihPodatkov = new ArrayList<>();

        listPotrebnihPodatkov.add(izbraniVnosi);
        listPotrebnihPodatkov.add(vrednostIzbranihVnosov);

        return listPotrebnihPodatkov;
    }

    /**
     * Metodo uporabimo za procesiranje ajax zahteve, kjerse ov vsaki spremembi izbrane zbirke, diagnoze ali leta
     * izvede ta metoda, da vrne veljavne podatke, ki se prikažejo oz. posodobijo v spustem meniju.
     *
     * @param izbranaDiagnoza tipa String
     * @param izbranoLeto     tipa String
     * @param izbranaZbrika   tipa String
     * @return niziIzbraneZbirke tipa List<String>
     */
    @RequestMapping(value = "/posodobitev_spustnega_menija", method = RequestMethod.GET)
    @ResponseBody
    public List<String> posodobitevSpustnegaMenija(@RequestParam("izbranaDiagnoza") String izbranaDiagnoza,
                                                   @RequestParam("izbranoLeto") String izbranoLeto,
                                                   @RequestParam("izbranaZbirka") String izbranaZbrika) {
        List<String> niziIzbraneZbirke = new ArrayList<>();

        if (izbranaZbrika.equals("Izvajalci ZV V6")) {
            SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View inSkupinahSPPV6View = new SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View();
            SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 skupinahSPPV6 = zvInSkupinahSPPV6Service
                    .getSPPPrimerPoIzvajalcihZVInSkupinahSPPV6ByLetoAndOznakaInImeBolezni(izbranoLeto, izbranaDiagnoza);

            niziIzbraneZbirke = inSkupinahSPPV6View.vrniListImenValidnihIzvajalcevZV(skupinahSPPV6);
        } else if (izbranaZbrika.equals("Statistične regije bivališča V6")) {
            SPPPrimerPoStatisticnihRegijahBivaliscaV6View bivaliscaV6View = new SPPPrimerPoStatisticnihRegijahBivaliscaV6View();
            SPPPrimerPoStatisticnihRegijahBivaliscaV6 regijahBivaliscaV6 = statisticnihRegijahBivaliscaV6Service
                    .getSPPPrimerPoStatisticnihRegijahBivaliscaV6ByLetoAndOznakaInImeBolezni(izbranoLeto, izbranaDiagnoza);

            niziIzbraneZbirke = bivaliscaV6View.vrniListValidnihRegij(regijahBivaliscaV6);
        }
        return niziIzbraneZbirke;
    }

    /**
     * Metodo uporabimo za procesiranje ajax zahteve. Kot parametra dobimo izbrano bolezen, leto,
     * izvajalce zv s katerimi iz baze pridobimo pravi objekt in izbrano regijo ter vrnemo potrebne podatke za prikaz
     * ali posodobitev pie-charta na strani "iskanje_po_zbirkah.html".
     *
     * @param izbranaDiagnoza tipa String
     * @param izbranoLeto     tipa String
     * @param izbranaRegija   tipa String
     * @return listPotrebnihPodatkov tipa List<List>
     */
    @RequestMapping(value = "/posodobitev_piegrafa", method = RequestMethod.GET)
    @ResponseBody
    public List<List> posodobitevPieGrafa(@RequestParam("izbranaDiagnoza") String izbranaDiagnoza,
                                          @RequestParam("izbranoLeto") String izbranoLeto,
                                          @RequestParam("izbranaRegija") String izbranaRegija) {

        //Definiramo Arraylista, ki ju napolnimo z bolnicami, ki pripadajo izbrani regiji in pripadajačo vrednost
        List<Double> vrednostIzbranihVnosov = new ArrayList<>();
        List<String> bolnice = new ArrayList<>();

        //Definiramo SPPPrimerPoIzvajalcihZVInSkupinahSPPV6, ki vsebuje podatke o bolezni, letu in vseh izvajalcih

        SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 a = zvInSkupinahSPPV6Service
                .getSPPPrimerPoIzvajalcihZVInSkupinahSPPV6ByLetoAndOznakaInImeBolezni(izbranoLeto, izbranaDiagnoza);


        //Najprej preverimo izbrano regijo, nato pa v objektu najdemo vse bolnice, ki pripadajo določeni regiji. Podatke damo v arraylist.

        if (izbranaRegija.equalsIgnoreCase("Skupaj")) {

            if (!a.getSplosnaBolnisnicaMurskaSobota().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Murska Sobota");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaMurskaSobota()));
            }


            if (!a.getSplosnaBolnisnicaPtuj().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Ptuj");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaPtuj()));
            }


            if (!a.getUkcMaribor().equalsIgnoreCase("-")) {
                bolnice.add("UKC Maribor");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getUkcMaribor()));
            }


            if (!a.getKirurskiCenterTos().equalsIgnoreCase("-")) {
                bolnice.add("Kirurški center Toš");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getKirurskiCenterTos()));
            }


            if (!a.getSplosnaBolnisnicaSlovenjGradec().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Slovenj Gradec");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaSlovenjGradec()));
            }
            if (!a.getBolnisnicaTopolsnica().equalsIgnoreCase("-")) {
                bolnice.add("Bolnišnica Topolšica");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getBolnisnicaTopolsnica()));
            }

            if (!a.getSplosnaBolnisnicaCelje().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Celje");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaCelje()));
            }
            if (!a.getSplosnaBolnisnicaTrbovlje().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Trbovlje");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaTrbovlje()));
            }
            if (!a.getSplosnaBolnisnicaBrezice().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Brežice");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaBrezice()));
            }
            if (!a.getSplosnaBolnisnicaNovoMesto().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Novo Mesto");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaNovoMesto()));
            }
            if (!a.getOnkoloskiInstitutLjubljana().equalsIgnoreCase("-")) {
                bolnice.add("Onkološki inštitut Ljubljana");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getOnkoloskiInstitutLjubljana()));
            }

            if (!a.getUkcLjubljana().equalsIgnoreCase("-")) {
                bolnice.add("UKC Ljubljana");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getUkcLjubljana()));
            }

            if (!a.getArborMea().equalsIgnoreCase("-")) {
                bolnice.add("Arbor Mea");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getArborMea()));
            }

            if (!a.getArtros().equalsIgnoreCase("-")) {
                bolnice.add("Artros");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getArtros()));
            }

            if (!a.getEstetikaFabjan().equalsIgnoreCase("-")) {
                bolnice.add("Estetska kirurgija Fabjan");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getEstetikaFabjan()));
            }

            if (!a.getKirurgijaBitenc().equalsIgnoreCase("-")) {
                bolnice.add("Kirurgija Bitenc");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getKirurgijaBitenc()));
            }

            if (!a.getKirurskiSenatorijRoznaDolina().equalsIgnoreCase("-")) {
                bolnice.add("Kirurški senatorij Rožna dolina");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getKirurskiSenatorijRoznaDolina()));
            }

            if (!a.getMcIatros().equalsIgnoreCase("-")) {
                bolnice.add("MC Iatros");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getMcIatros()));
            }

            if (!a.getMcMedicor().equalsIgnoreCase("-")) {
                bolnice.add("MC Medicor");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getMcMedicor()));
            }
            if (!a.getBolnisnicaKranj().equalsIgnoreCase("-")) {
                bolnice.add("Bolnišnica za gin. in porod. Kranj");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getBolnisnicaKranj()));
            }

            if (!a.getKlinikaGolnik().equalsIgnoreCase("-")) {
                bolnice.add("KOPA Golnik");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getKlinikaGolnik()));
            }

            if (!a.getSplosnaBolnisnicaJesenice().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Jesenice");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaJesenice()));
            }
            if (!a.getBolnisnicaSezana().equalsIgnoreCase("-")) {
                bolnice.add("Bolnišnica Sežana");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getBolnisnicaSezana()));
            }

            if (!a.getOrtopedskaBolnisnicaValdoltra().equalsIgnoreCase("-")) {
                bolnice.add("Ortopedska bolnišnica Valdoltra");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getOrtopedskaBolnisnicaValdoltra()));
            }

            if (!a.getSplosnaBolnisnicaIzola().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Izola");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaIzola()));
            }

            if (!a.getZdravstveniZavodzaKardiovaskularnodejavnost().equalsIgnoreCase("-")) {
                bolnice.add("Zdravstveni zavod za kardiovaskularno dejavnost");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getZdravstveniZavodzaKardiovaskularnodejavnost()));
            }

            if (!a.getBolnisnicaPostojna().equalsIgnoreCase("-")) {
                bolnice.add(" Bolnišnica za ž.b. in porod. Postojna");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getBolnisnicaPostojna()));
            }
            if (!a.getSplosnaBolnisnicaNovaGorica().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Nova Gorica");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaNovaGorica()));
            }

        }


        if (izbranaRegija.equalsIgnoreCase("Pomurska")) {

            if (!a.getSplosnaBolnisnicaMurskaSobota().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Murska Sobota");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaMurskaSobota()));
            }

        }

        if (izbranaRegija.equalsIgnoreCase("Podravska")) {


            if (!a.getSplosnaBolnisnicaPtuj().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Ptuj");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaPtuj()));
            }


            if (!a.getUkcMaribor().equalsIgnoreCase("-")) {
                bolnice.add("UKC Maribor");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getUkcMaribor()));
            }

            if (!a.getKirurskiCenterTos().equalsIgnoreCase("-")) {
                bolnice.add("Kirurški center Toš");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getKirurskiCenterTos()));
            }


        }

        if (izbranaRegija.equalsIgnoreCase("Koroška")) {

            if (!a.getSplosnaBolnisnicaSlovenjGradec().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Slovenj Gradec");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaSlovenjGradec()));
            }

        }

        if (izbranaRegija.equalsIgnoreCase("Savinjska")) {

            if (!a.getBolnisnicaTopolsnica().equalsIgnoreCase("-")) {
                bolnice.add("Bolnišnica Topolšica");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getBolnisnicaTopolsnica()));
            }

            if (!a.getSplosnaBolnisnicaCelje().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Celje");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaCelje()));
            }

        }

        if (izbranaRegija.equalsIgnoreCase("Zasavska")) {

            if (!a.getSplosnaBolnisnicaTrbovlje().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Trbovlje");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaTrbovlje()));
            }


        }

        if (izbranaRegija.equalsIgnoreCase("Posavska")) {

            if (!a.getSplosnaBolnisnicaBrezice().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Brežice");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaBrezice()));
            }


        }

        if (izbranaRegija.equalsIgnoreCase("Jugovzhodna Slovenija")) {

            if (!a.getSplosnaBolnisnicaNovoMesto().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Novo Mesto");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaNovoMesto()));
            }


        }


        if (izbranaRegija.equalsIgnoreCase("Osrednjeslovenska")) {

            if (!a.getOnkoloskiInstitutLjubljana().equalsIgnoreCase("-")) {
                bolnice.add("Onkološki inštitut Ljubljana");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getOnkoloskiInstitutLjubljana()));
            }

            if (!a.getUkcLjubljana().equalsIgnoreCase("-")) {
                bolnice.add("UKC Ljubljana");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getUkcLjubljana()));
            }

            if (!a.getArborMea().equalsIgnoreCase("-")) {
                bolnice.add("Arbor Mea");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getArborMea()));
            }

            if (!a.getArtros().equalsIgnoreCase("-")) {
                bolnice.add("Artros");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getArtros()));
            }

            if (!a.getEstetikaFabjan().equalsIgnoreCase("-")) {
                bolnice.add("Estetska kirurgija Fabjan");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getEstetikaFabjan()));
            }

            if (!a.getKirurgijaBitenc().equalsIgnoreCase("-")) {
                bolnice.add("Kirurgija Bitenc");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getKirurgijaBitenc()));
            }

            if (!a.getKirurskiSenatorijRoznaDolina().equalsIgnoreCase("-")) {
                bolnice.add("Kirurški senatorij Rožna dolina");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getKirurskiSenatorijRoznaDolina()));
            }

            if (!a.getMcIatros().equalsIgnoreCase("-")) {
                bolnice.add("MC Iatros");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getMcIatros()));
            }

            if (!a.getMcMedicor().equalsIgnoreCase("-")) {
                bolnice.add("MC Medicor");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getMcMedicor()));
            }

        }


        if (izbranaRegija.equalsIgnoreCase("Gorenjska")) {

            if (!a.getBolnisnicaKranj().equalsIgnoreCase("-")) {
                bolnice.add("Bolnišnica za gin. in porod. Kranj");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getBolnisnicaKranj()));
            }

            if (!a.getKlinikaGolnik().equalsIgnoreCase("-")) {
                bolnice.add("KOPA Golnik");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getKlinikaGolnik()));
            }

            if (!a.getSplosnaBolnisnicaJesenice().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Jesenice");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaJesenice()));
            }


        }

        if (izbranaRegija.equalsIgnoreCase("Obalno-kraška")) {

            if (!a.getBolnisnicaSezana().equalsIgnoreCase("-")) {
                bolnice.add("Bolnišnica Sežana");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getBolnisnicaSezana()));
            }

            if (!a.getOrtopedskaBolnisnicaValdoltra().equalsIgnoreCase("-")) {
                bolnice.add("Ortopedska bolnišnica Valdoltra");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getOrtopedskaBolnisnicaValdoltra()));
            }

            if (!a.getSplosnaBolnisnicaIzola().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Izola");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaIzola()));
            }

            if (!a.getZdravstveniZavodzaKardiovaskularnodejavnost().equalsIgnoreCase("-")) {
                bolnice.add("Zdravstveni zavod za kardiovaskularno dejavnost");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getZdravstveniZavodzaKardiovaskularnodejavnost()));
            }

            if (!a.getBolnisnicaPostojna().equalsIgnoreCase("-")) {
                bolnice.add(" Bolnišnica za ž.b. in porod. Postojna");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getBolnisnicaPostojna()));
            }

        }


        if (izbranaRegija.equalsIgnoreCase("Goriška")) {

            if (!a.getSplosnaBolnisnicaNovaGorica().equalsIgnoreCase("-")) {
                bolnice.add("Splošna bolnišnica Nova Gorica");
                vrednostIzbranihVnosov.add(Double.valueOf(a.getSplosnaBolnisnicaNovaGorica()));
            }


        }

        Double vsota = 0.0;
        for (Double stevilo : vrednostIzbranihVnosov) {
            vsota = vsota + stevilo;
        }

        DecimalFormat df = new DecimalFormat("0.00");


        List<Double> vrednostivProcentih = new ArrayList<>();
        for (Double stevilo : vrednostIzbranihVnosov) {
            //vrednostivProcentih.add(((stevilo/vsota)*100));

            vrednostivProcentih.add(Math.round(((stevilo / vsota) * 100) * 100.0) / 100.0);
        }


        List<List> listPotrebnihPodatkov = new ArrayList<>();

        listPotrebnihPodatkov.add(bolnice);
        listPotrebnihPodatkov.add(vrednostivProcentih);


        return listPotrebnihPodatkov;
    }


    /**
     * Metodo uporabimo za procesiranje ajax zahteve. Kot parametra dobimo izbrano bolezen, leto in
     * izvajalce zv s katerimi iz baze pridobimo pravi objekt, ter vrnemo potrebne podatke za prikaz
     * ali posodobitev grafa na strani "iskanje_po_diagnozah.html".
     *
     * @param izbranaDiagnoza tipa String
     * @param izbraniVnosi    tipa List<String>
     * @return listPotrebnihPodatkov tipa List<List>
     */
    @RequestMapping(value = "/posodobitevLineCharta", method = RequestMethod.GET)
    @ResponseBody
    public List<List> posodobitevLineCharta(@RequestParam("izbranaDiagnoza") String izbranaDiagnoza,

                                            @RequestParam("izbraniVnosi[]") List<String> izbraniVnosi) {

        List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> list = new ArrayList<>();
        List<List> vrednostIzbranihVnosov = new ArrayList<>();

        list = statisticnihRegijahBivaliscaV6Service.getBolezniZaLeto(izbranaDiagnoza);


        if (izbraniVnosi.contains("Skupaj")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getSkupaj().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }


        if (izbraniVnosi.contains("Pomurska")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getPomurska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }

        if (izbraniVnosi.contains("Podravska")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getPodravska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }
        if (izbraniVnosi.contains("Koroška")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getKoroska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }
        if (izbraniVnosi.contains("Savinjska")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getSavinjska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }
        if (izbraniVnosi.contains("Zasavska")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getZasavska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }
        if (izbraniVnosi.contains("Spodnjeposavska")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getPosavska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }
        if (izbraniVnosi.contains("Jugovzhodna Slovenija")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getJugovzhodnaSlovenija().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }
        if (izbraniVnosi.contains("Osrednjeslovenska")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getOsrednjeslovenska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }
        if (izbraniVnosi.contains("Gorenjska")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getGorenjska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }
        if (izbraniVnosi.contains("Notranjo-kraška")) {
            List<String> vrednostiZaRegijo = new ArrayList<>();
            for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 a : list) {

                vrednostiZaRegijo.add(a.getPrimorskoNotranjska().get(0));
            }
            vrednostIzbranihVnosov.add(vrednostiZaRegijo);
        }


        List<Integer> leta = new ArrayList<>();
        leta.add(2013);
        leta.add(2014);
        leta.add(2015);
        leta.add(2016);
        leta.add(2017);
        leta.add(2018);


        List<List> listPotrebnihPodatkov = new ArrayList<>();


        listPotrebnihPodatkov.add(leta);
        listPotrebnihPodatkov.add(izbraniVnosi);
        listPotrebnihPodatkov.add(vrednostIzbranihVnosov);


        return listPotrebnihPodatkov;
    }


}