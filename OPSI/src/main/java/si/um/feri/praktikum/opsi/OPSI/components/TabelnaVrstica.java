package si.um.feri.praktikum.opsi.OPSI.components;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoStatisticnihRegijahBivaliscaV6;
import si.um.feri.praktikum.opsi.OPSI.services.SPPPrimerPoStatisticnihRegijahBivaliscaV6Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public class TabelnaVrstica {

    private SPPPrimerPoStatisticnihRegijahBivaliscaV6Service service;

    private String diagnoza;
    private String steviloPrimerov;

    @Autowired
    public TabelnaVrstica(SPPPrimerPoStatisticnihRegijahBivaliscaV6Service service) {
        this.service = service;
    }

    public TabelnaVrstica(String diagnoza, String steviloPrimerov) {
        this.diagnoza = diagnoza;
        this.steviloPrimerov = steviloPrimerov;
    }

    /**
     * Glavna metoda, ki uporabi pomožne metode, da pridobi s podanim letom vse diagnoze in vrne 10 diagnoz, ki imajo
     * največ primerov.
     *
     * @param leto tipa String
     * @return tabelneVrstice tipa List<TabelnaVrstica>
     */
    public List<TabelnaVrstica> vrniPrvihDesetTabelnihVrsticDiagnozZNajvecPrimeri(String leto) {
        List<TabelnaVrstica> tabelneVrstice = new ArrayList<>();
        List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> diagnozeGledeNaLeto = service.getAllOznakaInImeBolezniAndSkupajByLeto(leto);
        List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> filtriraneDiagnoze = vrniFiltriraneDiagnoze(diagnozeGledeNaLeto);
        Collections.sort(filtriraneDiagnoze);
        List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> desetFiltriranihDiagnoz = vrniDesetFiltriranihDiagnoz(filtriraneDiagnoze);
        for (SPPPrimerPoStatisticnihRegijahBivaliscaV6 diagnoza : desetFiltriranihDiagnoz) {
            tabelneVrstice.add(new TabelnaVrstica(
                    diagnoza.getOznakaInImeBolezni().substring(5),
                    diagnoza.getSkupaj().get(0).substring(0, diagnoza.getSkupaj().get(0).length() - 2)));
        }
        return tabelneVrstice;
    }

    /**
     * Metoda vrne filtrirane diagnoze.
     *
     * @param diagnozeGledeNaLeto tipa List<SPPPrimerPoStatisticnihRegijahBivaliscaV6>
     * @return List<SPPPrimerPoStatisticnihRegijahBivaliscaV6>
     */
    private List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> vrniFiltriraneDiagnoze(List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> diagnozeGledeNaLeto) {
        return diagnozeGledeNaLeto.stream()
                .filter(d -> !d.getSkupaj().get(0).equals("-") && !d.getOznakaInImeBolezni().equals("SPP skupine-SKUPAJ"))
                .collect(Collectors.toList());
    }

    /**
     * Metoda nam izmed vseh podanih diagnoz vrne 10 razvrščenih diagnoz, ki imajo največ primerov.
     *
     * @param filtriraneDiagnoze tipa List<SPPPrimerPoStatisticnihRegijahBivaliscaV6>
     * @return desetFiltriranihDiagnoz List<SPPPrimerPoStatisticnihRegijahBivaliscaV6>
     */
    private List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> vrniDesetFiltriranihDiagnoz(List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> filtriraneDiagnoze) {
        List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> desetFiltriranihDiagnoz = new ArrayList<>();
        int indeks = 0;
        for (int i = filtriraneDiagnoze.size() - 1; i > 0; i--) {
            indeks++;
            if (indeks < 6)
                desetFiltriranihDiagnoz.add(filtriraneDiagnoze.get(i));
            else
                break;
        }
        return desetFiltriranihDiagnoz;
    }

}
