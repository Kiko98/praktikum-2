package si.um.feri.praktikum.opsi.OPSI.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoStatisticnihRegijahBivaliscaV6;

import java.util.List;

public interface SPPPrimerPoStatisticnihRegijahBivaliscaV6Repository
        extends MongoRepository<SPPPrimerPoStatisticnihRegijahBivaliscaV6, Long> {

    SPPPrimerPoStatisticnihRegijahBivaliscaV6 getSPPPrimerPoStatisticnihRegijahBivaliscaV6ByLetoAndOznakaInImeBolezni(String leto, String oznakaInImeBolezni);

    List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> getSPPPrimerPoStatisticnihRegijahBivaliscaV6ByOznakaInImeBolezni(String oznakaInImeBolezni);

    @Query(value="{'leto' : ?0 }", fields="{'oznakaInImeBolezni' : 1}")
    List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> getAllOznakaInImeBolezniByLeto(String leto);

    @Query(value="{'leto' : ?0 }", fields="{'oznakaInImeBolezni' : 1, 'skupaj' : 1}")
    List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> getAllOznakaInImeBolezniAndSkupajByLeto(String leto);

}
