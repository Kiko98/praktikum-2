package si.um.feri.praktikum.opsi.OPSI.views.helpers;

import java.util.*;

public class Pomagac {

    /**
     * Metoda preverja, če niz ni eden izmed spodaj podanih znakov. V primeru, da je, vrne false, drugače pa true.
     *
     * @param niz tipa String
     * @return vrnemo true tipa boolean, če niz ni eden izmed spodnjih znakov
     */
    public boolean preveriCeJeVrednostStevilo(String niz) {
        return !niz.equals("-") && !niz.equals("...") && !niz.equals(" ");
    }

    /**
     * V list "vrednostiIzbranihVnosov" dodajamo številčne vrednosti izbranih vnosov, ki se nahajajo v listu
     * "izbraniVnosi", tako da prečesamo skozi HashMap "map" in primerjamo niz HashMap-a in niz v listu "izbraniVnosi".
     *
     * @param map tipa HashMap<String, Integer>
     * @param izbraniVnosi tipa List<String>
     * @return vrednostiIzbranihVnosov tipa List<Integer>
     */
    public List<Integer> vrniListVrednostiIzbranihVnosov(LinkedHashMap<String, Integer> map, List<String> izbraniVnosi) {
        List<Integer> vrednostiIzbranihVnosov = new ArrayList<>();
        for (Map.Entry<String, Integer> vnos : map.entrySet()) {
            for (String izbranVnos : izbraniVnosi) {
                if (izbranVnos.equals(vnos.getKey()))
                    vrednostiIzbranihVnosov.add(vnos.getValue());
            }
        }
        return vrednostiIzbranihVnosov;
    }

}
