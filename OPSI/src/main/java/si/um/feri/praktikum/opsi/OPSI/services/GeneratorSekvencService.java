package si.um.feri.praktikum.opsi.OPSI.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import si.um.feri.praktikum.opsi.OPSI.models.BaznaSekvenca;

import java.util.Objects;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class GeneratorSekvencService {

    private MongoOperations mongoOperations;

    @Autowired
    public GeneratorSekvencService(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    public long generirajSekvenco(String seqName) {
        BaznaSekvenca stevec = mongoOperations.findAndModify(query(where("_id").is(seqName)),
                new Update().inc("seq",1), options().returnNew(true).upsert(true),
                BaznaSekvenca.class);
        return !Objects.isNull(stevec) ? stevec.getSeq() : 1;
    }
}
