package si.um.feri.praktikum.opsi.OPSI.models;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "bazne_sekvence")
public class BaznaSekvenca {

    @Id
    private String id;
    private int seq;

}
