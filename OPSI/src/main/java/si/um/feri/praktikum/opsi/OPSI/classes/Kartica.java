package si.um.feri.praktikum.opsi.OPSI.classes;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Kartica {

    private String nakljucnaDiagnoza;
    private String nakljucnaDiagnozaBrezOznake;
    private List<String> nakljucniVnosiRegij;

    public Kartica() {}

    public Kartica(String nakljucnaDiagnoza, String nakljucnaDiagnozaBrezOznake, List<String> nakljucniVnosiRegijAliIzvajalcev) {
        this.nakljucnaDiagnoza = nakljucnaDiagnoza;
        this.nakljucnaDiagnozaBrezOznake = nakljucnaDiagnozaBrezOznake;
        this.nakljucniVnosiRegij = nakljucniVnosiRegijAliIzvajalcev;
    }

    /**
     * Metoda nam v organizirani obliki izpiše vse regije.
     *
     * @return String
     */
    public String izpisiVnoseRegij() {
        StringBuilder niz = new StringBuilder();
        for (int i = 0; i < this.nakljucniVnosiRegij.size(); i++) {
            if (i != this.nakljucniVnosiRegij.size() - 1) {
                niz.append(this.nakljucniVnosiRegij.get(i));
                niz.append(", ");
            } else
                niz.append(this.nakljucniVnosiRegij.get(i));
        }
        return niz.toString();
    }

    /**
     * Metoda nam vrne list izbranih diagnoz iz katerih izberemo 6 za prikaz na karticah.
     *
     * @return diagnozeZaPrikazNaKarticah tipa List<String>
     */
    public List<String> vrniDiagnozeZaPrikazNaKarticah() {
        List<String> diagnozeZaPrikazNaKarticah = new ArrayList<>();
        diagnozeZaPrikazNaKarticah.add("O60Z Vaginalni porod");
        diagnozeZaPrikazNaKarticah.add("G10B Operacija kile brez spremljajočih bolezenskih stanj ali zapletov");
        diagnozeZaPrikazNaKarticah.add("D63Z Vnetje srednjega ušesa in vnetje zgornjih dihal");
        diagnozeZaPrikazNaKarticah.add("F62B Odpoved srca in šok brez katastrofalnih spremljajočih bolezenskih stanj ali zapletov");
        diagnozeZaPrikazNaKarticah.add("R63Z Kemoterapija");
        diagnozeZaPrikazNaKarticah.add("T63Z Virusna bolezen");
        diagnozeZaPrikazNaKarticah.add("P67D Novorojenček, teža ob sprejemu > 2499 g brez pomembnih posegov v operacijski dvorani brez težav");
        diagnozeZaPrikazNaKarticah.add("D10Z Posegi na nosu");
        diagnozeZaPrikazNaKarticah.add("L64Z Ledvični kamni in obstrukcija");
        diagnozeZaPrikazNaKarticah.add("F74Z Bolečina v prsih");
        diagnozeZaPrikazNaKarticah.add("B77Z Glavobol");
        diagnozeZaPrikazNaKarticah.add("I74Z Poškodbe podlakti, zapestja, dlani ali stopala");
        diagnozeZaPrikazNaKarticah.add("J10Z Plastika kože, podkožnega tkiva in dojke v operacijski dvorani");
        diagnozeZaPrikazNaKarticah.add("H60B Ciroza in alkoholni heptatitis z resnimi ali zmernimi spremljajočimi bolezenskimi stanji ali zapleti");
        diagnozeZaPrikazNaKarticah.add("L63B Infekcija ledvic in sečil brez katastrofalnih ali resnih spremljajočih bolezenskih stanj ali zapletov");
        return diagnozeZaPrikazNaKarticah;
    }

}
