package si.um.feri.praktikum.opsi.OPSI.models;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "spp_primeri_po_izvajalcih_zv_in_skupinah_spp_v6")
public class SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 {

    @Transient
    public final String IME_SEKVENCE = "spp_primeri_po_izvajalcih_zv_in_skupinah_spp_v6";

    @Id
    private Long id;
    private String leto;
    private String oznakaInImeBolezni;
    private String izvajalciZVSkupaj;
    private String arborMea;
    private String artros;
    private String bolnisnicaSezana;
    private String bolnisnicaTopolsnica;
    private String bolnisnicaKranj;
    private String bolnisnicaPostojna;
    private String estetikaFabjan;
    private String kirurskiCenterTos;
    private String kirurgijaBitenc;
    private String kirurskiSenatorijRoznaDolina;
    private String klinikaGolnik;
    private String mcIatros;
    private String mcMedicor;
    private String onkoloskiInstitutLjubljana;
    private String ortopedskaBolnisnicaValdoltra;
    private String splosnaBolnisnicaBrezice;
    private String splosnaBolnisnicaCelje;
    private String splosnaBolnisnicaIzola;
    private String splosnaBolnisnicaJesenice;
    private String splosnaBolnisnicaMurskaSobota;
    private String splosnaBolnisnicaNovaGorica;
    private String splosnaBolnisnicaNovoMesto;
    private String splosnaBolnisnicaPtuj;
    private String splosnaBolnisnicaSlovenjGradec;
    private String splosnaBolnisnicaTrbovlje;
    private String ukcLjubljana;
    private String ukcMaribor;
    private String zdravstveniZavodzaKardiovaskularnodejavnost;

    public SPPPrimerPoIzvajalcihZVInSkupinahSPPV6(String leto, String oznakaInImeBolezni, String izvajalciZVSkupaj,
                                                  String arborMea, String artros, String bolnisnicaSezana, String bolnisnicaTopolsnica,
                                                  String bolnisnicaKranj, String bolnisnicaPostojna, String estetikaFabjan,
                                                  String kirurskiCenterTos, String kirurgijaBitenc, String kirurskiSenatorijRoznaDolina,
                                                  String klinikaGolnik, String mcIatros, String mcMedicor, String onkoloskiInstitutLjubljana,
                                                  String ortopedskaBolnisnicaValdoltra, String splosnaBolnisnicaBrezice, String splosnaBolnisnicaCelje,
                                                  String splosnaBolnisnicaIzola, String splosnaBolnisnicaJesenice, String splosnaBolnisnicaMurskaSobota,
                                                  String splosnaBolnisnicaNovaGorica, String splosnaBolnisnicaNovoMesto, String splosnaBolnisnicaPtuj,
                                                  String splosnaBolnisnicaSlovenjGradec, String splosnaBolnisnicaTrbovlje,
                                                  String ukcLjubljana, String ukcMaribor, String zdravstveniZavodzaKardiovaskularnodejavnost) {
        this.leto = leto;
        this.oznakaInImeBolezni = oznakaInImeBolezni;
        this.izvajalciZVSkupaj = izvajalciZVSkupaj;
        this.arborMea = arborMea;
        this.artros = artros;
        this.bolnisnicaSezana = bolnisnicaSezana;
        this.bolnisnicaTopolsnica = bolnisnicaTopolsnica;
        this.bolnisnicaKranj = bolnisnicaKranj;
        this.bolnisnicaPostojna = bolnisnicaPostojna;
        this.estetikaFabjan = estetikaFabjan;
        this.kirurskiCenterTos = kirurskiCenterTos;
        this.kirurgijaBitenc = kirurgijaBitenc;
        this.kirurskiSenatorijRoznaDolina = kirurskiSenatorijRoznaDolina;
        this.klinikaGolnik = klinikaGolnik;
        this.mcIatros = mcIatros;
        this.mcMedicor = mcMedicor;
        this.onkoloskiInstitutLjubljana = onkoloskiInstitutLjubljana;
        this.ortopedskaBolnisnicaValdoltra = ortopedskaBolnisnicaValdoltra;
        this.splosnaBolnisnicaBrezice = splosnaBolnisnicaBrezice;
        this.splosnaBolnisnicaCelje = splosnaBolnisnicaCelje;
        this.splosnaBolnisnicaIzola = splosnaBolnisnicaIzola;
        this.splosnaBolnisnicaJesenice = splosnaBolnisnicaJesenice;
        this.splosnaBolnisnicaMurskaSobota = splosnaBolnisnicaMurskaSobota;
        this.splosnaBolnisnicaNovaGorica = splosnaBolnisnicaNovaGorica;
        this.splosnaBolnisnicaNovoMesto = splosnaBolnisnicaNovoMesto;
        this.splosnaBolnisnicaPtuj = splosnaBolnisnicaPtuj;
        this.splosnaBolnisnicaSlovenjGradec = splosnaBolnisnicaSlovenjGradec;
        this.splosnaBolnisnicaTrbovlje = splosnaBolnisnicaTrbovlje;
        this.ukcLjubljana = ukcLjubljana;
        this.ukcMaribor = ukcMaribor;
        this.zdravstveniZavodzaKardiovaskularnodejavnost = zdravstveniZavodzaKardiovaskularnodejavnost;
    }

    public int compareTo(SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 o)
    {
        return(Integer.parseInt(izvajalciZVSkupaj) - Integer.parseInt(o.izvajalciZVSkupaj));
    }
}
