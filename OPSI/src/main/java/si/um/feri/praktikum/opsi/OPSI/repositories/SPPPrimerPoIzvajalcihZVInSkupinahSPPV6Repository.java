package si.um.feri.praktikum.opsi.OPSI.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoIzvajalcihZVInSkupinahSPPV6;

import java.util.List;

public interface SPPPrimerPoIzvajalcihZVInSkupinahSPPV6Repository
        extends MongoRepository<SPPPrimerPoIzvajalcihZVInSkupinahSPPV6, Long> {

    SPPPrimerPoIzvajalcihZVInSkupinahSPPV6
    getSPPPrimerPoIzvajalcihZVInSkupinahSPPV6ByLetoAndOznakaInImeBolezni(String leto, String oznakaInImeBolezni);

    @Query(value="{'leto' : ?0 }", fields="{'oznakaInImeBolezni' : 1,'IzvajalciZVSkupaj' : 1}")
    List<SPPPrimerPoIzvajalcihZVInSkupinahSPPV6> getAllOznakaInImeBolezniByIdOrderByIzvajalciZVSkupajDesc(String leto);

}
