package si.um.feri.praktikum.opsi.OPSI.readers;

import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoIzvajalcihZVInSkupinahSPPV6;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoStatisticnihRegijahBivaliscaV6;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Getter
@Setter
public class SteviloSPPPrimerovBranjeDatotek {

    //private List<SPPPrimerPoIzvajalcihZVInSkupinahSPP> podatkiZaIzvajalceZV;
    private List<SPPPrimerPoIzvajalcihZVInSkupinahSPPV6> podatkiZaIzvajalceZVV6;
    //private List<SPPPrimerPoStatisticnihRegijahBivalisca> podatkiZaStatisticneRegije;
    private List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> podatkiZaStatisticneRegijeV6;

    public SteviloSPPPrimerovBranjeDatotek() {
        //this.podatkiZaIzvajalceZV = new ArrayList<>();
        this.podatkiZaIzvajalceZVV6 = new ArrayList<>();
        //this.podatkiZaStatisticneRegije = new ArrayList<>();
        this.podatkiZaStatisticneRegijeV6 = new ArrayList<>();
    }

    public void preberiPodatke(String tipZbirke, String leto, String potDoDatoteke) throws IOException {
        File file = new File(potDoDatoteke);
        FileInputStream fis = new FileInputStream(file);

        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);

        Iterator<Row> rowIterator = sheet.iterator();

        int indeks = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();

            List<Cell> celice = new ArrayList<>();
            if (tipZbirke.equals("Izvajalci ZV") || tipZbirke.equals("Izvajalci ZV v6")) {
                if (indeks != 0) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        celice.add(cell);
                    }
                    if (tipZbirke.equals("Izvajalci ZV")) {
                        /*this.podatkiZaIzvajalceZV.add(new SPPPrimerPoIzvajalcihZVInSkupinahSPP(leto, celice.get(0).toString(), celice.get(1).toString(), celice.get(2).toString(),
                                celice.get(3).toString(), celice.get(4).toString(), celice.get(5).toString(), celice.get(6).toString(),
                                celice.get(7).toString(), celice.get(8).toString(), celice.get(9).toString(), celice.get(10).toString(),
                                celice.get(11).toString(), celice.get(12).toString(), celice.get(13).toString(), celice.get(14).toString(),
                                celice.get(15).toString(), celice.get(16).toString(), celice.get(17).toString(), celice.get(18).toString(),
                                celice.get(19).toString(), celice.get(20).toString(), celice.get(21).toString(), celice.get(22).toString(),
                                celice.get(23).toString(), celice.get(24).toString(), celice.get(25).toString(), celice.get(26).toString(),
                                celice.get(27).toString(), celice.get(28).toString()));*/
                    } else {
                        this.podatkiZaIzvajalceZVV6.add(new SPPPrimerPoIzvajalcihZVInSkupinahSPPV6(leto, celice.get(0).toString(), celice.get(1).toString(), celice.get(2).toString(),
                                celice.get(3).toString(), celice.get(4).toString(), celice.get(5).toString(), celice.get(6).toString(),
                                celice.get(7).toString(), celice.get(8).toString(), celice.get(9).toString(), celice.get(10).toString(),
                                celice.get(11).toString(), celice.get(12).toString(), celice.get(13).toString(), celice.get(14).toString(),
                                celice.get(15).toString(), celice.get(16).toString(), celice.get(17).toString(), celice.get(18).toString(),
                                celice.get(19).toString(), celice.get(20).toString(), celice.get(21).toString(), celice.get(22).toString(),
                                celice.get(23).toString(), celice.get(24).toString(), celice.get(25).toString(), celice.get(26).toString(),
                                celice.get(27).toString(), celice.get(28).toString(), celice.get(29).toString()));
                    }
                }
            } else if (tipZbirke.equals("Statisticne regije") || tipZbirke.equals("Statisticne regije v6")) {
                if (indeks != 0 && indeks != 1) {
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        celice.add(cell);
                    }
                    List<String> skupaj = new ArrayList<>();
                    skupaj.add(celice.get(1).toString());
                    skupaj.add(celice.get(2).toString());
                    skupaj.add(celice.get(3).toString());
                    List<String> pomurska = new ArrayList<>();
                    pomurska.add(celice.get(4).toString());
                    pomurska.add(celice.get(5).toString());
                    pomurska.add(celice.get(6).toString());
                    List<String> podravska = new ArrayList<>();
                    podravska.add(celice.get(7).toString());
                    podravska.add(celice.get(8).toString());
                    podravska.add(celice.get(9).toString());
                    List<String> koroska = new ArrayList<>();
                    koroska.add(celice.get(10).toString());
                    koroska.add(celice.get(11).toString());
                    koroska.add(celice.get(12).toString());
                    List<String> savinjska = new ArrayList<>();
                    savinjska.add(celice.get(13).toString());
                    savinjska.add(celice.get(14).toString());
                    savinjska.add(celice.get(15).toString());
                    List<String> zasavska = new ArrayList<>();
                    zasavska.add(celice.get(16).toString());
                    zasavska.add(celice.get(17).toString());
                    zasavska.add(celice.get(18).toString());
                    List<String> spodnjeposavska = new ArrayList<>();
                    spodnjeposavska.add(celice.get(19).toString());
                    spodnjeposavska.add(celice.get(20).toString());
                    spodnjeposavska.add(celice.get(21).toString());
                    List<String> jugovzhodnaSlovenija = new ArrayList<>();
                    jugovzhodnaSlovenija.add(celice.get(22).toString());
                    jugovzhodnaSlovenija.add(celice.get(23).toString());
                    jugovzhodnaSlovenija.add(celice.get(24).toString());
                    List<String> osrednjeslovenska = new ArrayList<>();
                    osrednjeslovenska.add(celice.get(25).toString());
                    osrednjeslovenska.add(celice.get(26).toString());
                    osrednjeslovenska.add(celice.get(27).toString());
                    List<String> gorenjska = new ArrayList<>();
                    gorenjska.add(celice.get(28).toString());
                    gorenjska.add(celice.get(29).toString());
                    gorenjska.add(celice.get(30).toString());
                    List<String> notranjskoKraska = new ArrayList<>();
                    notranjskoKraska.add(celice.get(31).toString());
                    notranjskoKraska.add(celice.get(32).toString());
                    notranjskoKraska.add(celice.get(33).toString());
                    List<String> goriska = new ArrayList<>();
                    goriska.add(celice.get(34).toString());
                    goriska.add(celice.get(35).toString());
                    goriska.add(celice.get(36).toString());
                    List<String> obalnoKraska = new ArrayList<>();
                    obalnoKraska.add(celice.get(37).toString());
                    obalnoKraska.add(celice.get(38).toString());
                    obalnoKraska.add(celice.get(39).toString());
                    List<String> tujina = new ArrayList<>();
                    tujina.add(celice.get(40).toString());
                    tujina.add(celice.get(41).toString());
                    tujina.add(celice.get(42).toString());
                    List<String> neznano = new ArrayList<>();
                    neznano.add(celice.get(43).toString());
                    neznano.add(celice.get(44).toString());
                    neznano.add(celice.get(45).toString());
                    if (tipZbirke.equals("Statisticne regije")) {
                        /*this.podatkiZaStatisticneRegije.add(new SPPPrimerPoStatisticnihRegijahBivalisca(leto, celice.get(0).toString(), skupaj, pomurska, podravska,
                                koroska, savinjska, zasavska, spodnjeposavska, jugovzhodnaSlovenija, osrednjeslovenska,
                                gorenjska, notranjskoKraska, goriska, obalnoKraska, tujina, neznano));*/
                    } else {
                        this.podatkiZaStatisticneRegijeV6.add(new SPPPrimerPoStatisticnihRegijahBivaliscaV6(leto, celice.get(0).toString(), skupaj, pomurska, podravska,
                                koroska, savinjska, zasavska, spodnjeposavska, jugovzhodnaSlovenija, osrednjeslovenska,
                                gorenjska, notranjskoKraska, goriska, obalnoKraska, tujina, neznano));
                    }
                }
            }
            indeks++;
        }
        workbook.close();
        fis.close();
    }
}
