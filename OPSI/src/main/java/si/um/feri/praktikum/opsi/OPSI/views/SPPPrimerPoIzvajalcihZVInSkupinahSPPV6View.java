package si.um.feri.praktikum.opsi.OPSI.views;

import lombok.Getter;
import lombok.Setter;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoIzvajalcihZVInSkupinahSPPV6;
import si.um.feri.praktikum.opsi.OPSI.views.helpers.Pomagac;

import java.util.*;

@Getter
@Setter
public class SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View {

    private List<String> imenaIzvajalcevZV;

    private Pomagac pomagac;

    public SPPPrimerPoIzvajalcihZVInSkupinahSPPV6View() {
        this.imenaIzvajalcevZV = new ArrayList<>();
        this.pomagac = new Pomagac();
        napolniImenaIzvajalcevZVVList();
    }

    /**
     * Ob inicializaciji objekta uporabimo to metodo, da napolnimo list z imeni vseh izvajalcev ZV.
     */
    private void napolniImenaIzvajalcevZVVList() {
        this.imenaIzvajalcevZV.add("Izvajalci-SKUPAJ");
        this.imenaIzvajalcevZV.add("Arbor mea");
        this.imenaIzvajalcevZV.add("Artros");
        this.imenaIzvajalcevZV.add("Bolnišnica Sežana");
        this.imenaIzvajalcevZV.add("Bolnišnica Topolšica");
        this.imenaIzvajalcevZV.add("Bolnišnica za ginekologijo in porodništvo Kranj");
        this.imenaIzvajalcevZV.add("Bolnišnica za ženske bolezni in porodništvo Postojna");
        this.imenaIzvajalcevZV.add("Estetska kirurgija Fabjan");
        this.imenaIzvajalcevZV.add("KC Toš");
        this.imenaIzvajalcevZV.add("Kirurugija Bitenc");
        this.imenaIzvajalcevZV.add("Kirurški sanatorij Rožna dolina");
        this.imenaIzvajalcevZV.add("Klinika Golnik");
        this.imenaIzvajalcevZV.add("MC Iatros");
        this.imenaIzvajalcevZV.add("MC Medicor");
        this.imenaIzvajalcevZV.add("Onkološki inštitut Ljubljana");
        this.imenaIzvajalcevZV.add("Ortopedska bolnišnica Valdoltra");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Brežice");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Celje");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Izola");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Jesenice");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Murska Sobota");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Nova Gorica");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Novo mesto");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Ptuj");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Slovenj Gradec");
        this.imenaIzvajalcevZV.add("Splošna bolnišnica Trbovlje");
        this.imenaIzvajalcevZV.add("UKC Ljubljana");
        this.imenaIzvajalcevZV.add("UKC Maribor");
        this.imenaIzvajalcevZV.add("Zdravstveni zavod za kardiovaskularno dejavnost");
    }

    /**
     * Metoda s pomočjo pomožne metode "preveriCeJeVrednostStevilo" preverja, če so vrednosti atributov podanega
     * objekta števila ali ne. V primeru, da je vrednost atributa v obliki niza število, shrani izvajalca, ki se
     * nanaša na določen atribut iz lista "imenaIzvajalcevZV" v list "validnaImenaIzvajalcevZV".
     *
     * @param sppv6 tipa SPPPrimerPoIzvajalcihZVInSkupinahSPPV6
     * @return validnaImenaIzvajalcevZV tipa List<String>
     */
    public List<String> vrniListImenValidnihIzvajalcevZV(SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 sppv6) {
        List<String> validnaImenaIzvajalcevZV = new ArrayList<>();
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getIzvajalciZVSkupaj()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(0));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getArborMea()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(1));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getArtros()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(2));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getBolnisnicaSezana()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(3));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getBolnisnicaTopolsnica()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(4));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getBolnisnicaKranj()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(5));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getBolnisnicaPostojna()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(6));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getEstetikaFabjan()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(7));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getKirurskiCenterTos()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(8));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getKirurgijaBitenc()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(9));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getKirurskiSenatorijRoznaDolina()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(10));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getKlinikaGolnik()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(11));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getMcIatros()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(12));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getMcMedicor()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(13));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getOnkoloskiInstitutLjubljana()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(14));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getOrtopedskaBolnisnicaValdoltra()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(15));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaBrezice()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(16));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaCelje()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(17));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaIzola()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(18));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaJesenice()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(19));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaMurskaSobota()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(20));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaNovaGorica()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(21));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaNovoMesto()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(22));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaPtuj()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(23));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaSlovenjGradec()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(24));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaTrbovlje()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(25));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getUkcLjubljana()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(26));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getUkcMaribor()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(27));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getZdravstveniZavodzaKardiovaskularnodejavnost()))
            validnaImenaIzvajalcevZV.add(imenaIzvajalcevZV.get(28));


        return validnaImenaIzvajalcevZV;
    }

    /**
     * Metoda ustvari HashMap<String, Integer>, kot niz shrani ime izvajalca iz lista "imenaIzvajalcevZV", kot
     * število pa pretvori vrednost atributa, ki se nanaša na izvajalca iz objekta iz niza v število. Ko je HashMap
     * ustvarjen ga s pomočjo metode "vrniListVrednostiIzbranihVnosov" iteriramo, primerjamo imena izvajalcev iz lista
     * "izbraniIzvajalciZV" in imena izvajalcev v HashMapu, če so imena ista shranimo številčne  vrednosti tega imena
     * iz HashMap-a v list "vrednostiIzbranihVnosov" v pomožni metodi in list vrnemo.
     *
     * @param sppv6              tipa SPPPrimerPoIzvajalcihZVInSkupinahSPPV6
     * @param izbraniIzvajalciZV tipa List<String>
     * @return List<Integer>
     */
    public List<Integer> vrniVrednostiIzbranihIzvajalcevZV(SPPPrimerPoIzvajalcihZVInSkupinahSPPV6 sppv6,
                                                           List<String> izbraniIzvajalciZV) {
        LinkedHashMap<String, Integer> vrednostiIzvajalcevZVHashMap = new LinkedHashMap<>();
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getIzvajalciZVSkupaj()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(0),
                    Math.round(Float.parseFloat(sppv6.getIzvajalciZVSkupaj())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getArborMea()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(1),
                    Math.round(Float.parseFloat(sppv6.getArborMea())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getArtros()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(2),
                    Math.round(Float.parseFloat(sppv6.getArtros())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getBolnisnicaSezana()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(3),
                    Math.round(Float.parseFloat(sppv6.getBolnisnicaSezana())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getBolnisnicaTopolsnica()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(4),
                    Math.round(Float.parseFloat(sppv6.getBolnisnicaTopolsnica())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getBolnisnicaKranj()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(5),
                    Math.round(Float.parseFloat(sppv6.getBolnisnicaKranj())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getBolnisnicaPostojna()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(6),
                    Math.round(Float.parseFloat(sppv6.getBolnisnicaPostojna())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getEstetikaFabjan()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(7),
                    Math.round(Float.parseFloat(sppv6.getEstetikaFabjan())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getKirurskiCenterTos()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(8),
                    Math.round(Float.parseFloat(sppv6.getKirurskiCenterTos())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getKirurgijaBitenc()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(9),
                    Math.round(Float.parseFloat(sppv6.getKirurgijaBitenc())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getKirurskiSenatorijRoznaDolina()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(10),
                    Math.round(Float.parseFloat(sppv6.getKirurskiSenatorijRoznaDolina())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getKlinikaGolnik()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(11),
                    Math.round(Float.parseFloat(sppv6.getKlinikaGolnik())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getMcIatros()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(12),
                    Math.round(Float.parseFloat(sppv6.getMcIatros())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getMcMedicor()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(13),
                    Math.round(Float.parseFloat(sppv6.getMcMedicor())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getOnkoloskiInstitutLjubljana()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(14),
                    Math.round(Float.parseFloat(sppv6.getOnkoloskiInstitutLjubljana())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getOrtopedskaBolnisnicaValdoltra()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(15),
                    Math.round(Float.parseFloat(sppv6.getOrtopedskaBolnisnicaValdoltra())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaBrezice()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(16),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaBrezice())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaCelje()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(17),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaCelje())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaIzola()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(18),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaIzola())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaJesenice()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(19),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaJesenice())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaMurskaSobota()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(20),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaMurskaSobota())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaNovaGorica()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(21),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaNovaGorica())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaNovoMesto()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(22),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaNovoMesto())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaPtuj()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(23),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaPtuj())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaSlovenjGradec()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(24),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaSlovenjGradec())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getSplosnaBolnisnicaTrbovlje()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(25),
                    Math.round(Float.parseFloat(sppv6.getSplosnaBolnisnicaTrbovlje())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getUkcLjubljana()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(26),
                    Math.round(Float.parseFloat(sppv6.getUkcLjubljana())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getUkcMaribor()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(27),
                    Math.round(Float.parseFloat(sppv6.getUkcMaribor())));
        if (pomagac.preveriCeJeVrednostStevilo(sppv6.getZdravstveniZavodzaKardiovaskularnodejavnost()))
            vrednostiIzvajalcevZVHashMap.put(imenaIzvajalcevZV.get(28),
                    Math.round(Float.parseFloat(sppv6.getZdravstveniZavodzaKardiovaskularnodejavnost())));
        return pomagac.vrniListVrednostiIzbranihVnosov(
                vrednostiIzvajalcevZVHashMap, izbraniIzvajalciZV);
    }

}
