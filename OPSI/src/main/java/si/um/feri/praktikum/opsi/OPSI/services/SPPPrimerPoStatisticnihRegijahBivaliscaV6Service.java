package si.um.feri.praktikum.opsi.OPSI.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import si.um.feri.praktikum.opsi.OPSI.models.SPPPrimerPoStatisticnihRegijahBivaliscaV6;
import si.um.feri.praktikum.opsi.OPSI.repositories.SPPPrimerPoStatisticnihRegijahBivaliscaV6Repository;

import java.util.List;

@Service
public class SPPPrimerPoStatisticnihRegijahBivaliscaV6Service {

    private final SPPPrimerPoStatisticnihRegijahBivaliscaV6Repository repo;

    @Autowired
    public SPPPrimerPoStatisticnihRegijahBivaliscaV6Service(SPPPrimerPoStatisticnihRegijahBivaliscaV6Repository repo) {
        this.repo = repo;
    }

    public List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> getAll() {
        return repo.findAll();
    }

    public List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> getBolezniZaLeto(String OznakaInImeBolezni){
        return repo.getSPPPrimerPoStatisticnihRegijahBivaliscaV6ByOznakaInImeBolezni(OznakaInImeBolezni);
    }

    public void save(SPPPrimerPoStatisticnihRegijahBivaliscaV6 sppPrimerPoStatisticnihRegijahBivaliscaV6) {
        repo.save(sppPrimerPoStatisticnihRegijahBivaliscaV6);
    }

    public SPPPrimerPoStatisticnihRegijahBivaliscaV6 getById(Long id) {
        return repo.findById(id).get();
    }

    public void deleteById(Long id) {
        repo.deleteById(id);
    }

    public List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> getAllOznakaInImeBolezniById(String leto) {
        return repo.getAllOznakaInImeBolezniByLeto(leto);
    }

    public SPPPrimerPoStatisticnihRegijahBivaliscaV6 getSPPPrimerPoStatisticnihRegijahBivaliscaV6ByLetoAndOznakaInImeBolezni
            (String leto, String oznakaInImeBolezni) {
        return repo.getSPPPrimerPoStatisticnihRegijahBivaliscaV6ByLetoAndOznakaInImeBolezni(leto, oznakaInImeBolezni);
    }

    public List<SPPPrimerPoStatisticnihRegijahBivaliscaV6> getAllOznakaInImeBolezniAndSkupajByLeto(String leto) {
        return repo.getAllOznakaInImeBolezniAndSkupajByLeto(leto);
    }

}
