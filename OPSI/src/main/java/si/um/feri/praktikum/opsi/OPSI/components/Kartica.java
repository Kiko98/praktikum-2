package si.um.feri.praktikum.opsi.OPSI.components;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.*;

@Getter
@Setter
@Component
public class Kartica {

    private String nakljucnaDiagnoza;
    private String skrajsanaNakljucnaDiagnoza;
    private List<String> regije;

    private List<String> diagnozeZaPrikazNaKarticahList;
    private List<String> regijeZaPrikazNaKarticahList;

    private HashMap<String, String> diagnozeZaPrikazNaKarticahHashMap;
    private HashMap<String, List<String>> regijeZaPrikazNaKarticahHashMap;

    public Kartica() {
        this.diagnozeZaPrikazNaKarticahList = vrniDiagnozeZaPrikazNaKarticahList();
        this.regijeZaPrikazNaKarticahList = vrniRegijeList();
        this.diagnozeZaPrikazNaKarticahHashMap = vrniDiagnozeZaPrikazNaKarticahHashMap();
        this.regijeZaPrikazNaKarticahHashMap = vrniRegijeHashMap();
    }

    private Kartica(String nakljucnaDiagnoza, String nakljucnaDiagnozaBrezOznake, List<String> regije) {
        this.nakljucnaDiagnoza = nakljucnaDiagnoza;
        this.skrajsanaNakljucnaDiagnoza = nakljucnaDiagnozaBrezOznake;
        this.regije = regije;
    }

    /**
     * Metoda nam v organizirani obliki izpiše vse regije.
     *
     * @return String
     */
    public String izpisiVnoseRegij() {
        StringBuilder niz = new StringBuilder();
        for (int i = 0; i < this.regije.size(); i++) {
            if (i != this.regije.size() - 1) {
                niz.append(this.regije.get(i));
                niz.append(", ");
            } else
                niz.append(this.regije.get(i));
        }
        return niz.toString();
    }

    /**
     * Metoda nam vrne list izbranih diagnoz iz katerih izberemo 6 za prikaz na karticah.
     *
     * @return diagnozeZaPrikazNaKarticahList tipa List<String>
     */
    private List<String> vrniDiagnozeZaPrikazNaKarticahList() {
        List<String> diagnozeZaPrikazNaKarticahList = new ArrayList<>();
        diagnozeZaPrikazNaKarticahList.add("O60Z Vaginalni porod");
        diagnozeZaPrikazNaKarticahList.add("G10B Operacija kile brez spremljajočih bolezenskih stanj ali zapletov");
        diagnozeZaPrikazNaKarticahList.add("D63Z Vnetje srednjega ušesa in vnetje zgornjih dihal");
        diagnozeZaPrikazNaKarticahList.add("F62B Odpoved srca in šok brez katastrofalnih spremljajočih bolezenskih stanj ali zapletov");
        diagnozeZaPrikazNaKarticahList.add("R63Z Kemoterapija");
        diagnozeZaPrikazNaKarticahList.add("T63Z Virusna bolezen");
        diagnozeZaPrikazNaKarticahList.add("P67D Novorojenček, teža ob sprejemu > 2499 g brez pomembnih posegov v operacijski dvorani brez težav");
        diagnozeZaPrikazNaKarticahList.add("D10Z Posegi na nosu");
        diagnozeZaPrikazNaKarticahList.add("L64Z Ledvični kamni in obstrukcija");
        diagnozeZaPrikazNaKarticahList.add("F74Z Bolečina v prsih");
        diagnozeZaPrikazNaKarticahList.add("B77Z Glavobol");
        diagnozeZaPrikazNaKarticahList.add("I74Z Poškodbe podlakti, zapestja, dlani ali stopala");
        diagnozeZaPrikazNaKarticahList.add("J10Z Plastika kože, podkožnega tkiva in dojke v operacijski dvorani");
        diagnozeZaPrikazNaKarticahList.add("H60B Ciroza in alkoholni heptatitis z resnimi ali zmernimi spremljajočimi bolezenskimi stanji ali zapleti");
        diagnozeZaPrikazNaKarticahList.add("L63B Infekcija ledvic in sečil brez katastrofalnih ali resnih spremljajočih bolezenskih stanj ali zapletov");
        return diagnozeZaPrikazNaKarticahList;
    }

    /**
     * Metoda nam vrne HashMap, ki kot ključ vsebuje diagnoze, kot vrednosti pa njim pripadajoče skrajšane diagnoze.
     *
     * @return diagnozeZaPrikazNaKarticahHashMap tipa HashMap<String, String>
     */
    private HashMap<String, String> vrniDiagnozeZaPrikazNaKarticahHashMap() {
        HashMap<String, String> diagnozeZaPrikazNaKarticahHashMap = new HashMap<>();
        diagnozeZaPrikazNaKarticahHashMap.put("O60Z Vaginalni porod", "Vaginalni porod");
        diagnozeZaPrikazNaKarticahHashMap.put("G10B Operacija kile brez spremljajočih bolezenskih stanj ali zapletov", "Operacija kile(pruh)");
        diagnozeZaPrikazNaKarticahHashMap.put("D63Z Vnetje srednjega ušesa in vnetje zgornjih dihal", "Vnetje srednjega ušesa in vnetje zgornjih dihal");
        diagnozeZaPrikazNaKarticahHashMap.put("F62B Odpoved srca in šok brez katastrofalnih spremljajočih bolezenskih stanj ali zapletov", "Odpoved srca in šok");
        diagnozeZaPrikazNaKarticahHashMap.put("R63Z Kemoterapija", "Kemoterapija");
        diagnozeZaPrikazNaKarticahHashMap.put("T63Z Virusna bolezen", "Virusna bolezen");
        diagnozeZaPrikazNaKarticahHashMap.put("P67D Novorojenček, teža ob sprejemu > 2499 g brez pomembnih posegov v operacijski dvorani brez težav", "Novorojenček, teža ob sprejemu > 2499 g ");
        diagnozeZaPrikazNaKarticahHashMap.put("D10Z Posegi na nosu", "Posegi na nosu");
        diagnozeZaPrikazNaKarticahHashMap.put("L64Z Ledvični kamni in obstrukcija", "Ledvični kamni in obstrukcija");
        diagnozeZaPrikazNaKarticahHashMap.put("F74Z Bolečina v prsih", "Bolečina v prsih");
        diagnozeZaPrikazNaKarticahHashMap.put("B77Z Glavobol", "Glavobol");
        diagnozeZaPrikazNaKarticahHashMap.put("I74Z Poškodbe podlakti, zapestja, dlani ali stopala", "Poškodbe podlakti, zapestja, dlani ali stopala");
        diagnozeZaPrikazNaKarticahHashMap.put("J10Z Plastika kože, podkožnega tkiva in dojke v operacijski dvorani", "Plastika kože, podkožnega tkiva in dojke");
        diagnozeZaPrikazNaKarticahHashMap.put("H60B Ciroza in alkoholni heptatitis z resnimi ali zmernimi spremljajočimi bolezenskimi stanji ali zapleti", "Ciroza in alkoholni heptatitis ");
        diagnozeZaPrikazNaKarticahHashMap.put("L63B Infekcija ledvic in sečil brez katastrofalnih ali resnih spremljajočih bolezenskih stanj ali zapletov", "Infekcija ledvic in sečil ");
        return diagnozeZaPrikazNaKarticahHashMap;
    }

    /**
     * Metoda vrne list izbranih regij.
     *
     * @return regije tipa List<String>
     */
    public List<String> vrniRegijeList() {
        List<String> regijeList = new ArrayList<>();
        regijeList.add("Skupaj");
        regijeList.add("Pomurska");
        regijeList.add("Podravska");
        regijeList.add("Koroška");
        regijeList.add("Savinjska");
        regijeList.add("Zasavska");
        regijeList.add("Spodnjeposavska");
        regijeList.add("Jugovzhodna Slovenija");
        regijeList.add("Osrednjeslovenska");
        regijeList.add("Gorenjska");
        regijeList.add("Notranjsko-kraška");
        regijeList.add("Goriška");
        regijeList.add("Obalno-kraška");
        return regijeList;
    }

    /**
     * Vrne HashMap, ki kot ključ vsebuje diagnoze, kot vrednosti pa njim pripadajoče izbrane regije.
     *
     * @return regijeHashMap tipa HashMap<String, List<String>>
     */
    private HashMap<String, List<String>> vrniRegijeHashMap() {
        HashMap<String, List<String>> regijeHashMap = new HashMap<>();
        regijeHashMap.put("O60Z Vaginalni porod", Arrays.asList("Pomurska", "Podravska", "Spodnjeposavska", "Osrednjeslovenska", "Jugovzhodna Slovenija"));
        regijeHashMap.put("G10B Operacija kile brez spremljajočih bolezenskih stanj ali zapletov", Arrays.asList("Podravska", "Savinjska", "Osrednjeslovenska", "Jugovzhodna Slovenija", "Gorenjska"));
        regijeHashMap.put("D63Z Vnetje srednjega ušesa in vnetje zgornjih dihal", Arrays.asList("Pomurska", "Podravska", "Spodnjeposavska", "Osrednjeslovenska", "Jugovzhodna Slovenija"));
        regijeHashMap.put("F62B Odpoved srca in šok brez katastrofalnih spremljajočih bolezenskih stanj ali zapletov", Arrays.asList("Zasavska", "Spodnjeposavska", "Osrednjeslovenska", "Jugovzhodna Slovenija", "Gorenjska"));
        regijeHashMap.put("R63Z Kemoterapija", Arrays.asList("Podravska", "Koroška", "Osrednjeslovenska", "Jugovzhodna Slovenija", "Gorenjska"));
        regijeHashMap.put("T63Z Virusna bolezen", Arrays.asList("Podravska", "Koroška", "Savinjska", "Jugovzhodna Slovenija", "Osrednjeslovenska"));
        regijeHashMap.put("P67D Novorojenček, teža ob sprejemu > 2499 g brez pomembnih posegov v operacijski dvorani brez težav", Arrays.asList("Podravska", "Savinjska", "Jugovzhodna Slovenija", "Osrednjeslovenska", "Gorenjska"));
        regijeHashMap.put("D10Z Posegi na nosu", Arrays.asList("Podravska", "Savinjska", "Jugovzhodna Slovenija", "Osrednjeslovenska", "Gorenjska"));
        regijeHashMap.put("L64Z Ledvični kamni in obstrukcija", Arrays.asList("Pomurska", "Spodnjeposavska", "Osrednjeslovenska", "Jugovzhodna Slovenija", "Gorenjska"));
        regijeHashMap.put("F74Z Bolečina v prsih", Arrays.asList("Pomurska", "Koroška", "Savinjska", "Jugovzhodna Slovenija", "Gorenjska"));
        regijeHashMap.put("B77Z Glavobol", Arrays.asList("Podravska", "Koroška", "Savinjska", "Jugovzhodna Slovenija", "Osrednjeslovenska"));
        regijeHashMap.put("I74Z Poškodbe podlakti, zapestja, dlani ali stopala", Arrays.asList("Podravska", "Koroška", "Savinjska", "Jugovzhodna Slovenija", "Osrednjeslovenska"));
        regijeHashMap.put("J10Z Plastika kože, podkožnega tkiva in dojke v operacijski dvorani", Arrays.asList("Podravska", "Savinjska", "Jugovzhodna Slovenija", "Osrednjeslovenska", "Gorenjska"));
        regijeHashMap.put("H60B Ciroza in alkoholni heptatitis z resnimi ali zmernimi spremljajočimi bolezenskimi stanji ali zapleti", Arrays.asList("Pomurska", "Podravska", "Savinjska", "Osrednjeslovenska", "Gorenjska"));
        regijeHashMap.put("L63B Infekcija ledvic in sečil brez katastrofalnih ali resnih spremljajočih bolezenskih stanj ali zapletov", Arrays.asList("Podravska", "Savinjska", "Osrednjeslovenska"));
        return regijeHashMap;
    }

    /**
     * Metoda nam vrne iz podanega lista diagnoz 6 naključnih diagnoz in njej pripadajoče skrajšane diagnoze in regije.
     *
     * @return kartice tipa List<Kartica>
     */
    public List<Kartica> vrniObjekteKarticZaPrikazNaDomaciStrani() {
        List<Kartica> kartice = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            String nakljucnaDiagnoza = vrniNakljucniVnos(this.diagnozeZaPrikazNaKarticahList);
            kartice.add(new Kartica(nakljucnaDiagnoza,
                    this.diagnozeZaPrikazNaKarticahHashMap.get(nakljucnaDiagnoza),
                    this.regijeZaPrikazNaKarticahHashMap.get(nakljucnaDiagnoza)));
        }
        return kartice;
    }

    /**
     * Metoda nam iz podanega lista vnosov, vrnemo en naključni vnos.
     *
     * @param vnosi List<String>
     * @return tipa String
     */
    private String vrniNakljucniVnos(List<String> vnosi) {
        Random random = new Random();
        int nakljucnoStevilo = random.nextInt(vnosi.size());
        String vnos = vnosi.get(nakljucnoStevilo);
        vnosi.remove(nakljucnoStevilo);
        return vnos;
    }

}
