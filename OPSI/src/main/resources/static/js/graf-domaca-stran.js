/**
 * Konfiguracija in prikaz grafa za "desktop"
 */
new Chart(document.getElementById("line-chart"), {
    type: 'line',
    data: {
        labels: [2013, 2014, 2015, 2016, 2017, 2018],
        datasets: [{
            data: [196, 265, 275, 279, 276, 455],
            label: "Koroška",
            borderColor: "#fcba03",
            fill: false,
            pointRadius: 5
        }, {
            data: [134, 110, 80, 156, 203, 360],
            label: "Pomurska",
            borderColor: "#ffdd00",
            fill: false,
            pointRadius: 5
        }, {
            data: [640, 706, 905, 900, 1016,1890],
            label: "Osrednjeslovenska",
            borderColor: "#ff7b00",
            fill: false,
            pointRadius: 5
        }, {
            data: [123, 160, 153, 149, 235, 383],
            label: "Jugovzhodna Slovenija",
            borderColor: "#ff5900",
            fill: false,
            pointRadius: 5
        }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            labels: {
                fontColor: 'white'
            }
        },
        title: {
            display: true,
            text: 'Prikaz števila primerov po regijah za diagnozo "Kemoterapija"',
            fontColor: 'white'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    fontColor: 'white'
                },
            }],
            xAxes: [{
                ticks: {
                    fontColor: 'white',
                },
            }]
        }
    }
});

/**
 * Konfiguracija in prikaz grafa za "phone"
 */
new Chart(document.getElementById("line-chart-phone"), {
    type: 'line',
    data: {
        labels: [2013, 2014, 2015, 2016, 2017, 2018],
        datasets: [{
            data: [196, 265, 275, 279, 276, 455],
            label: "Koroška",
            borderColor: "#fcba03",
            fill: false,
            pointRadius: 5
        }, {
            data: [134, 110, 80, 156, 203, 360],
            label: "Pomurska",
            borderColor: "#ffdd00",
            fill: false,
            pointRadius: 5
        }, {
            data: [640, 706, 905, 900, 1016,1890],
            label: "Osrednjeslovenska",
            borderColor: "#ff7b00",
            fill: false,
            pointRadius: 5
        }, {
            data: [123, 160, 153, 149, 235, 383],
            label: "Jugovzhodna Slovenija",
            borderColor: "#ff5900",
            fill: false,
            pointRadius: 5
        }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            labels: {
                fontColor: 'white'
            }
        },
        title: {
            display: true,
            text: 'Prikaz števila primerov po regijah za diagnozo "Kemoterapija"',
            fontColor: 'white'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    fontColor: 'white'
                },
            }],
            xAxes: [{
                ticks: {
                    fontColor: 'white',
                },
            }]
        }
    }
});