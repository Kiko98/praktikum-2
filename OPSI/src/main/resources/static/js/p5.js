const particles = [];

/**
 * Natsvimo "canvas", kjer bo animacija potekali, torej določimo višino in širino in na podlagi tega
 * določimo velikost "particlov". Za tem jih pushamo.
 */
function setup() {
    let canvas = createCanvas(window.innerWidth, window.innerHeight);
    canvas.parent("particles");

    const particlesLength = Math.floor(window.innerWidth / 10);

    for (let i = 0; i < particlesLength; i++) {
        particles.push(new Particle());
    }
}

/**
 * S pomočjo metod v razredu jih v vsakem frame-u vedno znova posodabljamo, da se premikajo. Torej
 * prvo posodobimo pozicijo, kaneje ga narišemo na tisto pozicijo in preverimo, odddaljenost določenega
 * "particla" z ostalimi. Če sta dovolj blizu ju povžemo z ravno črto.
 */
function draw() {
    background(34, 34, 34);
    particles.forEach((p, index) => {
        p.update();
        p.draw();
        p.checkParticles(particles.slice(index));
    });
}

/**
 * Ustvarjen razred, ki vsebuje vse potrebne metode. En razred predstavlja en "particle" v animaciji.
 */
class Particle {
    /**
     * V konstruktorju ustvarimo dva naključna vektorja za pozicijo in hitrost, ter določimo še velikost.
     */
    constructor() {
        this.pozicija = createVector(random(width), random(height));
        this.hitrost = createVector(random(-2, 2), random(-2, 2));
        this.velikost = 10;
    }

    /**
     * S pomočjo te metode "particle" vedno znova posodabljamo, da dobimo animacijo premikanja, to storimo
     * tako da dodajamo vrednost hitrosti.
     */
    update() {
        this.pozicija.add(this.hitrost);
        this.edges();
    }

    /**
     * Metoda se uporabi po metodi "update" in sicer, ko je pozicija določena, ga narišemo oz premaknemo
     * na to mesto.
     */
    draw() {
        noStroke();
        fill('rgba(247, 140, 0, 1)');
        circle(this.pozicija.x, this.pozicija.y, this.velikost);
    }

    /**
     * Preverjamo robove, če "particle" pride do roba ekrana se odbije od njega.
     */
    edges() {
        if (this.pozicija.x < 5 || this.pozicija.x > width - 5) {
            this.hitrost.x *= -1;
        }

        if (this.pozicija.y < 5 || this.pozicija.y > height - 5) {
            this.hitrost.y *= -1;
        }
    }

    /**
     * Z metodo preverjamo, če sta dva "particla" dovolj blizu skupaj, da ju povežemo s črto.
     *
     * @param particles
     */
    checkParticles(particles) {
        particles.forEach(particle => {
            const d = dist(this.pozicija.x, this.pozicija.y, particle.pozicija.x, particle.pozicija.y);

            if (d < 120) {
                stroke('rgba(247, 140, 0, 0.2)');
                strokeWeight(1);
                line(this.pozicija.x, this.pozicija.y, particle.pozicija.x, particle.pozicija.y)
            }
        })
    }
}