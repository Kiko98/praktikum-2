/**
 * Določimo globalno spremenljivko myChart kjer so shranjeni podatki
 * za prikaz grafa, podatki se spreminjajo ob vsaki posodobitvi oz.
 * izbiri filtrov in potrditvi prikaza.
 */
var myChart;
var prikazGrafa = 0;

/**
 * Ob kliku na gumb z id-jem potrdi_izbiro pridobimo vrednost izbrane bolezni, leta in izbranih
 * vnosov glede na izbrano zbirko v obliki niza. Za tem izvedemo ajax zahtevo, kjer ob uspešni izvedbi zahteve
 * pridobimo potrebne podatke za prikaz le tih na grafu. Če grafa še ni ga na novo ustvarimo,
 * drugače pa ga samo posodobimo.
 */
$("#potrdi_izbiro").click(function () {
    if ($("#myChart").length) {
        myChart.destroy();
    }
    var izbranaDiagnoza = $('#select-diagnoza').val();
    var izbraniVnosi = $('#select-regije').val();
    $.ajax({
        type: "GET",
        url: "/posodobitevLineCharta",
        data: {
            izbranaDiagnoza: izbranaDiagnoza,
            izbraniVnosi: izbraniVnosi
        },
        success: function (podatki) {
            prikazGrafa++;
            var datasetValue = [];
            var count = podatki[1].length;
            for (var j = 0; j < count; j++) {
                datasetValue[j] = {
                    label: podatki[1][j],
                    data: podatki[2][j],
                    showLine: true,
                    fill: false,
                    borderColor: getRandomColor()
                }
            }
            var mydata = {
                labels: podatki[0],
                datasets: datasetValue
            };
            var data = {
                labels: podatki[3],
                datasets: [{
                    label: podatki[1][0],
                    data: podatki[1],
                    showLine: true,
                    fill: false,
                    borderColor: 'rgba(0, 200, 0, 1)'
                }, {
                    label: podatki[0][1],
                    data: podatki[5],
                    showLine: true,
                    fill: false,
                    borderColor: 'rgba(200, 0, 0, 1)'
                }
                ],
            };
            //var data = vrniPodatkeZaGraf(podatki);
            var options = vrniKonfiguracijoZaGraf(izbranaDiagnoza);
            var type = 'line';
            if (prikazGrafa === 1) {
                var canvas = $("<canvas id=\"myChart\"></canvas>");
                var html = '';
                html += '<div class="row">\n' +
                    '        <div class="col-12" align="center">\n' +
                    '            <a id="download" href="" class="btn btn-success active mt-4" download="crtni_diagram.jpg" role="button" aria-pressed="true">Izvozi graf</a>' +
                    '            <div class="graf" style="width: auto;">\n' +
                    '            </div>\n' +
                    '        </div>\n' +
                    '    </div>';
                $("#grafi").append(html);
                $(".graf").append(canvas);
            }
            var ctx = document.getElementById('myChart').getContext('2d');
            myChart = new Chart(ctx, {
                type: type,
                data: mydata,
                options: options
            });
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    })
});

/**
 * Metoda nam vrne 'options' konfiguracijo za prikaz grafa.
 *
 * @param izbranaDiagnoza
 * @returns {{legend: {display: boolean}, scales: {yAxes: {ticks: {beginAtZero: boolean}}[]}, title: {display: boolean, text: *}}}
 */

function vrniKonfiguracijoZaGraf(izbranaDiagnoza) {
    return {
        legend: {
            display: true,
            position: 'bottom'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        title: {
            display: true,
            text: izbranaDiagnoza
        }
    }
}

/**
 * Metoda nam vrne 'data' konfiguracijo za prikaz grafa.
 *
 * @param podatki
 * @returns {{datasets: {backgroundColor: string[], data: *, label: string}[], labels: *}}
 */
function vrniPodatkeZaGraf(podatki) {
    return {
        labels: podatki[0],
        datasets: [{
            label: 'Število primerov',
            data: podatki[1],
            backgroundColor: [getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor()]
        }]
    }
}

/**
 * Metoda nam vrne tip grafa.
 *
 * @return {string}
 */
function vrniTipGrafa() {
    return 'bar';
}

/**
 * Metoda nam vrne random barvo.
 *
 * @return {string}
 */
function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

/**
 * Metoda za izvoz stolpicnega grafa kot sliko.
 */
$(document).ready(function () {
    $("body").on('click', '#download', function () {
        var image = document.getElementById("myChart").toDataURL("image/jpg", "image/octet-stream");
        var a = document.getElementById("download");
        a.href = image;
    });
});

