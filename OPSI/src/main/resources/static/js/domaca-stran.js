/**
 * Metoda se izvede ob spremembi izbire spustnega menija, kjer izberemo leto. Ob spremembi se izvede ajax zahteva,
 * kjer dobimo podatke za podano oz. izbrano leto in posodobimo tabelo z ustreznimi pridobljenimi podatki.
 */
$("#select-leto").on('change', function () {
    let izbranoLeto = $('#select-leto').val();
    $.ajax({
        type: "GET",
        url: "/posodobitev_tabele",
        data: {izbranoLeto: izbranoLeto},
        success: function (data) {
            $("#tabela-diagnoz tr").not(":first").remove();
            let html = '';
            let count = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr><th class="transparentna-tabela" scope="row">' + count + '</th><td class="transparentna-tabela">'
                    + data[i].diagnoza + '</td><td class="transparentna-tabela">' + data[i].steviloPrimerov + '</td></tr>';
                count = count + 1;
            }
            $("#tabela-diagnoz tbody").after(html);
        }
    })
});

/**
 * Delovanje metode isto, responsive za telefon.
 */
$("#select-leto-phone").on('change', function () {
    let izbranoLeto = $('#select-leto-phone').val();
    $.ajax({
        type: "GET",
        url: "/posodobitev_tabele",
        data: {izbranoLeto: izbranoLeto},
        success: function (data) {
            $("#tabela-diagnoz-phone tr").not(":first").remove();
            let html = '';
            let count = 1;
            for (let i = 0; i < data.length; i++) {
                html += '<tr><th class="transparentna-tabela" scope="row">' + count + '</th><td class="transparentna-tabela">'
                    + data[i].diagnoza + '</td><td class="transparentna-tabela">' + data[i].steviloPrimerov + '</td></tr>';
                count = count + 1;
            }
            $("#tabela-diagnoz-phone tbody").after(html);
        }
    })
});

/**
 * Če je širina ekrana manjša od 992 skrijemo zadnji dve kartici, drugače pa prikažemo.
 */
if ($(window).width() < 992) {
    $("#5").hide();
    $("#6").hide();
} else {
    $("#5").show();
    $("#6").show();
}