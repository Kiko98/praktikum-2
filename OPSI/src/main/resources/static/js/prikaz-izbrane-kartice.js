/**
 * Ko je dokument pripravljen dobimo url, in iz njega potrebne parametre s pomočjo katerih select
 * elementom v html dokumentu predefiniramo vrednosti.
 */
$(document).ready(function () {
    const url = window.location.search;
    const urlParametri = new URLSearchParams(url);
    const diagnoza = urlParametri.get("diagnoza");
    const regije = urlParametri.getAll("regije");

    if (diagnoza.length > 0 && regije.length > 0) {
        $("#select-diagnoza").val(diagnoza).trigger("change");
        $("#select-regije").val(regije).trigger("change");
        $("#potrdi_izbiro").trigger('click');
    }
});