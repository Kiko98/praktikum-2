$(document).ready(function () {
    /**
     * Za select s podanim razredom se nam izvede funkcija iz vključene knjižnice select2
     * select2().
     */
    $('.js-example-basic-single').select2({
        theme: "bootstrap"
    });

    /**
     * Za select s podanim razredom se nam izvede funkcija iz vključene knjižnice select2
     * select2(), ki vključuje še dodatno konfiguracijo.
     */
    $('.js-example-basic-multiple').select2({
        theme: "bootstrap",
        multiple: true,
        placeholder: "Izberi...",
        allowClear: true,
        maximumSelectionLength: 5,
        language: {
            noResults: function () {
                return "Ni rezultatov";
            },
            maximumSelected: function () {
                return "Izberete lahko samo 5 izvajalcev ZV"
            }
        }
    });
});
