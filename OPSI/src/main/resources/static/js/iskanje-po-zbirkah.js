/**
 * Določimo globalno spremenljivko myChart kjer so shranjeni podatki
 * za prikaz grafa, podatki se spreminjajo ob vsaki posodobitvi oz.
 * izbiri filtrov in potrditvi prikaza.
 */
var myChart;
var myChart1;
var prikazGrafa = 0;

/**
 * Ko izberemo "radio" gumb za iskanje po zbirki izvajalcev, se nam posodobi ime spustnega seznama in njegova vsebina,
 * glede na izbrano zbirko
 */
$("#izvajalci-zv-radio").click(function () {
    var izbranaDiagnoza = $('#select-diagnoza').val();
    var izbranoLeto = $('#select-leto').val();
    if ($("#izvajalci-zv-radio").is(':checked')) {
        $.ajax({
            type: "GET",
            url: "/posodobitev_spustnega_menija",
            data: {izbranaDiagnoza: izbranaDiagnoza, izbranoLeto: izbranoLeto, izbranaZbirka: "Izvajalci ZV V6"},
            success: function (data) {
                $("#label-izvajalci-regije").text("Izberite izvajalce ZV");
                var elt = $('#select-izvajalci-regije');
                elt.empty();
                for (var i = 0; i < data.length; i++) {
                    var option = new Option(data[i], data[i], false, false);
                    elt.append(option).trigger('change');
                }
            }
        })
    }
});

/**
 * Ko izberemo "radio" gumb za iskanje po zbirki regij, se nam posodobi ime spustnega seznama in njegova vsebina,
 * glede na izbrano zbirko.
 */
$("#regije-radio").click(function () {
    var izbranaDiagnoza = $('#select-diagnoza').val();
    var izbranoLeto = $('#select-leto').val();
    if ($("#regije-radio").is(':checked')) {
        $.ajax({
            type: "GET",
            url: "/posodobitev_spustnega_menija",
            data: {
                izbranaDiagnoza: izbranaDiagnoza,
                izbranoLeto: izbranoLeto,
                izbranaZbirka: "Statistične regije bivališča V6"
            },
            success: function (data) {
                $("#label-izvajalci-regije").text("Izberite statistične regije bivališča");
                var elt = $('#select-izvajalci-regije');
                elt.empty();
                for (var i = 0; i < data.length; i++) {
                    var option = new Option(data[i], data[i], false, false);
                    elt.append(option).trigger('change');
                }
            }
        })
    }
});

/**
 * Ob vsaki spremembi na izbiri diagnoze v dropdown listu, se nam posoobi seznam
 * vnosov glede na izbrano zbirko, ker nimajo vsi vnosi za določeno diagnozo validne podatke.
 */
$("#select-diagnoza").on('change', function () {
    var izbranaZbirka;
    if ($("#izvajalci-zv-radio").is(':checked'))
        izbranaZbirka = "Izvajalci ZV V6";
    else if ($("#regije-radio").is(':checked'))
        izbranaZbirka = "Statistične regije bivališča V6";
    var izbranaDiagnoza = $('#select-diagnoza').val();
    var izbranoLeto = $('#select-leto').val();
    $.ajax({
        type: "GET",
        url: "/posodobitev_spustnega_menija",
        data: {izbranaDiagnoza: izbranaDiagnoza, izbranoLeto: izbranoLeto, izbranaZbirka: izbranaZbirka},
        success: function (data) {
            var elt = $('#select-izvajalci-regije');
            elt.empty();
            for (var i = 0; i < data.length; i++) {
                var option = new Option(data[i], data[i], false, false);
                elt.append(option).trigger('change');
            }
        }
    })
});

/**
 * Ob vsaki spremembi na izbiri leta v dropdown listu, se nam posoobi seznam
 * vnosov glede na izbrano zbirko, ker nimajo vsi vnosi za določeno diagnozo validne podatke.
 */
$("#select-leto").on('change', function () {
    var izbranaZbirka;
    if ($("#izvajalci-zv-radio").is(':checked'))
        izbranaZbirka = "Izvajalci ZV V6";
    else if ($("#regije-radio").is(':checked'))
        izbranaZbirka = "Statistične regije bivališča V6";
    var izbranaDiagnoza = $('#select-diagnoza').val();
    var izbranoLeto = $('#select-leto').val();
    $.ajax({
        type: "GET",
        url: "/posodobitev_spustnega_menija",
        data: {izbranaDiagnoza: izbranaDiagnoza, izbranoLeto: izbranoLeto, izbranaZbirka: izbranaZbirka},
        success: function (data) {
            var elt = $('#select-izvajalci-regije');
            elt.empty();
            for (var i = 0; i < data.length; i++) {
                var option = new Option(data[i], data[i], false, false);
                elt.append(option).trigger('change');
            }
        }
    })
});

/**
 * Ob kliku na gumb z id-jem potrdi_izbiro pridobimo vrednost izbrane bolezni, leta in izbranih
 * vnosov glede na izbrano zbirko v obliki niza. Za tem izvedemo ajax zahtevo, kjer ob uspešni izvedbi zahteve
 * pridobimo potrebne podatke za prikaz le tih na grafu. Če grafa še ni ga na novo ustvarimo,
 * drugače pa ga samo posodobimo.
 */
$("#potrdi_izbiro").click(function () {
    $("#grafi").empty();
    var izbranaZbirka;
    if ($("#izvajalci-zv-radio").is(':checked'))
        izbranaZbirka = "Izvajalci ZV V6";
    else if ($("#regije-radio").is(':checked'))
        izbranaZbirka = "Statistične regije bivališča V6";
    var izbranaDiagnoza = $('#select-diagnoza').val();
    var izbranoLeto = $('#select-leto').val();
    var izbraniVnosi = $('#select-izvajalci-regije').val();
    $.ajax({
        type: "GET",
        url: "/posodobitev_grafa",
        data: {
            izbranaDiagnoza: izbranaDiagnoza, izbranoLeto: izbranoLeto,
            izbraniVnosi: izbraniVnosi, izbranaZbirka: izbranaZbirka
        },
        success: function (podatki) {
            if ($("#myChart").length) {
                myChart.data.labels = podatki[0];
                myChart.data.datasets[0].data = podatki[1];
                myChart.options.title.text = izbranaDiagnoza;
                window.myChart.update();
            } else {
                var data = vrniPodatkeZaGraf(podatki);
                var options = vrniKonfiguracijoZaGraf(izbranaDiagnoza);
                var type = vrniTipGrafa();
                var canvas = $("<canvas id=\"myChart\" class=\"mt-4\"></canvas>");
                let html = '';
                if (izbranaZbirka === "Izvajalci ZV V6") {
                    html += '<div class="row">\n' +
                        '        <div class="col-12" align="center">\n' +
                        '            <a id="download" href=""  download="stolpicni_diagram.jpg" class="btn btn-success active mt-4" role="button" aria-pressed="true">Izvozi graf</a>' +
                        '            <div class="graf" style="width: auto;">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '    </div>'
                } else {
                    html += '<div class="row">\n' +
                        '        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" align="center">\n' +
                        '            <a id="download" href=""  download="stolpicni_diagram.jpg" class="btn btn-success active mt-4" role="button" aria-pressed="true">Izvozi graf</a>' +
                        '            <div class="graf" style="width: auto;">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" align="center">\n' +
                        '            <a id="download1" href=""  download="tortni_diagram.jpg" class="btn btn-success active mt-4" role="button" aria-pressed="true">Izvozi graf</a>' +
                        '            <div class="graf2" style="width: auto;">\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '    </div>'
                }
                $("#grafi").append(html);
                $(".graf").append(canvas);
                $("#download1").hide();
                var ctx = document.getElementById('myChart').getContext('2d');
                myChart = new Chart(ctx, {
                    type: type,
                    data: data,
                    options: options
                });
            }
        }
    })
});

/**
 * Metoda nam vrne 'data' konfiguracijo za prikaz grafa.
 *
 * @param podatki
 * @returns {{datasets: {backgroundColor: string[], data: *, label: string}[], labels: *}}
 */
function vrniPodatkeZaGraf(podatki) {
    return {
        labels: podatki[0],
        datasets: [{
            label: 'Število primerov',
            data: podatki[1],
            backgroundColor: [getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor, getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor, getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor, getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor, getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor, getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor()]
        }]
    }
}

/**
 * Metoda nam vrne 'options' konfiguracijo za prikaz grafa.
 *
 * @param izbranaDiagnoza
 * @returns {{legend: {display: boolean}, scales: {yAxes: {ticks: {beginAtZero: boolean}}[]}, title: {display: boolean, text: *}}}
 */
function vrniKonfiguracijoZaGraf(izbranaDiagnoza) {
    return {
        responsive: true,
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        title: {
            display: true,
            text: izbranaDiagnoza
        },
        onClick: function (evt, element) {
            //var activePoints = myChart.getElementAtEvent(evt);
            var activePoints = myChart.getElementsAtEventForMode(evt, 'point', myChart.options);
            var firstPoint = activePoints[0];
            var label = myChart.data.labels[firstPoint._index];
            var izbranaRegija = myChart.data.labels[firstPoint._index];
            var izbranaZbirka;
            if ($("#izvajalci-zv-radio").is(':checked'))
                izbranaZbirka = "Izvajalci ZV V6";
            else if ($("#regije-radio").is(':checked'))
                izbranaZbirka = "Statistične regije bivališča V6";
            var izbranaDiagnoza = $('#select-diagnoza').val();
            var izbranoLeto = $('#select-leto').val();
            var izbraniVnosi = $('#select-izvajalci-regije').val();
            $.ajax({
                type: "GET",
                url: "/posodobitev_piegrafa",
                data: {
                    izbranaDiagnoza: izbranaDiagnoza, izbranoLeto: izbranoLeto,
                    izbraniVnosi: izbraniVnosi, izbranaZbirka: izbranaZbirka,
                    izbranaRegija: izbranaRegija
                },
                success: function (podatki) {

                    $("#download1").show();
                    if ($("#myChart1").length) {
                        myChart1.data.labels = podatki[0];
                        myChart1.data.datasets[0].data = podatki[1];
                        myChart1.data = vrniPodatkeZaGraf(podatki);
                        window.myChart1.update();
                    } else {
                        var data = vrniPodatkeZaGraf(podatki);
                        var options = vrniKonfiguracijoZaGraf(izbranaDiagnoza);
                        var canvas1 = $("<canvas id=\"myChart1\" class=\"mt-4\"></canvas>");
                        $(".graf2").append(canvas1);
                        var ctx = document.getElementById('myChart1').getContext('2d');
                        myChart1 = new Chart(ctx, {
                            type: 'pie',
                            data: data,
                            options: {
                                responsive: true,
                                title: {
                                    display: true,

                                },
                                tooltips: {
                                    callbacks: {
                                        label: function (tooltipItem, data) {
                                            return data.labels[tooltipItem.index] + ' ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] + '%';
                                        }
                                    }
                                }
                            }
                        });
                    }
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                }
            })
        }
    }
}

/**
 * Metoda nam vrne tip grafa.
 *
 * @return {string}
 */
function vrniTipGrafa() {
    return 'bar';
}

/**
 * Metoda vrne nakljucno rgba barvo.
 *
 * @returns {string}
 */
function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

/**
 * Metoda za izvoz stolpicnega grafa kot sliko.
 */
$(document).ready(function () {
    $("body").on('click', '#download', function () {
        var image = document.getElementById("myChart").toDataURL("image/jpg", "image/octet-stream");
        var a = document.getElementById("download");
        a.href = image;
    });
});

/**
 * Metoda za izvoz tortnega grafa kot sliko.
 */
$(document).ready(function () {
    $("body").on('click', '#download1', function () {
        var url_base64jp = document.getElementById("myChart1").toDataURL("image/jpg");
        var a = document.getElementById("download1");
        a.href = url_base64jp;
    });
});
