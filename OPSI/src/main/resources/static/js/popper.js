/**
 * Knjižnico uporabimo za primaz "tooltipov".
 * Deklaracija potrebnih spremenljivk.
 */
const chart = document.getElementById('line-chart');
const tooltip = document.getElementById('tooltip');

const table = document.getElementById('tabela-diagnoz');
const tooltip1 = document.getElementById('tooltip1');

/**
 * Ustvarimo tooltip za graf.
 */
Popper.createPopper(chart, tooltip, {
    placement: 'left',
    modifiers: [
        {
            name: 'offset',
            options: {
                offset: [0, 8],
            },
        },
    ],
});

/**
 * Ustvarimo tooltip za tabelo.
 */
Popper.createPopper(table, tooltip1, {
    placement: 'right',
    modifiers: [
        {
            name: 'offset',
            options: {
                offset: [0, 8],
            },
        },
    ],
});

// /**
//  * Metoda za prikaz tooltipa ko hover-amo graf.
//  */
// function showChart() {
//     tooltip.setAttribute('data-show', '');
// }
//
// /**
//  * Metoda za skritje tooltipa ko un-hover-amo graf.
//  */
// function hideChart() {
//     tooltip.removeAttribute('data-show');
// }
//
// /**
//  * Metoda za prikaz tooltipa ko hover-amo tabelo.
//  */
// function showTable() {
//     tooltip1.setAttribute('data-show', '');
// }
// /**
//  * Metoda za skritje tooltipa ko un-hover-amo tabelo.
//  */
// function hideTable() {
//     tooltip1.removeAttribute('data-show');
// }
//
// const showEvents = ['mouseenter', 'focus'];
// const hideEvents = ['mouseleave', 'blur'];
//
// /**
//  * Sporti preverjamo stanje hover-a.
//  */
// showEvents.forEach(event => {
//     chart.addEventListener(event, showChart);
//     table.addEventListener(event, showTable);
// });
//
// /**
//  * Sporti preverjamo stanje hover-a.
//  */
// hideEvents.forEach(event => {
//     chart.addEventListener(event, hideChart);
//     table.addEventListener(event, hideTable);
// });
